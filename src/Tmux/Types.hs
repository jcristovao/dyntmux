{-# LANGUAGE DeriveDataTypeable   #-}
{-# LANGUAGE FlexibleInstances    #-}
{-# LANGUAGE OverlappingInstances #-}
{-# LANGUAGE DeriveGeneric        #-}
{-# OPTIONS_GHC -fno-warn-orphans #-}

module Tmux.Types where

import Data.Data
import Data.Int
import Data.Monoid
import Data.Maybe
import qualified Control.Newtype as NT
import GHC.Generics
import Data.Default.Generics
import Data.Text (Text)
import qualified Data.Text as T
import Control.Newtype

data SaveBehaviour = BlackListing | WhiteListing
  deriving (Eq,Show,Generic)

instance Default SaveBehaviour where def = BlackListing

data ProjectFormat = YamlTmuxinator | YamlDynTmux
  deriving (Eq, Show)
instance Default ProjectFormat where def = YamlDynTmux

data Configuration = Configuration
  { saveExtension :: ProjectFormat
  , saveBehaviour :: SaveBehaviour
  , saveList      :: [FilePath]
  } deriving (Eq,Show,Generic)

instance Default Configuration

type ScreenSize = (Int64,Int64)

instance Ord ScreenSize where
  compare (a0,a1) (b0,b1) = compare (a0 * a1) (b0 * b1)

data ShellEnv = ShellEnv
  { shellEnvName   :: FilePath
  , shellEnvEditor :: FilePath
  , shellEnvCwd    :: FilePath
  , shellTmux      :: FilePath
  } deriving (Eq, Show, Generic)

instance Default ShellEnv

data BaseIndex = BaseIndex
  { windowBaseIndex :: Int
  , paneBaseIndex   :: Int
  } deriving (Eq, Show,Generic)

instance Default BaseIndex

data Env = Env
  { screenSize    :: ScreenSize
  , colourOutput  :: Colour
  , sys           :: ShellEnv
  , baseIx        :: BaseIndex
  , configur      :: Configuration
  , defaultPath   :: Maybe FilePath
  , sessionName'  :: Maybe Text
  } deriving (Eq, Show, Generic)
instance Default Env

newtype Command  = Command { getCommand :: Text }
  deriving (Eq,Ord,Show,Typeable,Generic)
instance Newtype Command

type    Commands = [Command]

getSingleCommand :: [Command] -> Command
getSingleCommand [] = error "Empty list of commands"
getSingleCommand (c:[]) = c
getSingleCommand _ = error "List of commands has more than one element"

filePathToCommand :: FilePath -> Command
filePathToCommand = Command . T.pack

-- | Output colour messages to stdout?
newtype Colour  = Colour  { _colour  :: Bool } deriving (Eq,Show,Generic)
instance Default Colour
-- | Report environment analysis to stdout?
newtype Report  = Report  { _report  :: Bool } deriving (Eq)
-- | Lenient check on tmux version (and presence)?
newtype Lenient = Lenient { _lenient :: Bool } deriving (Eq)

tmuxPath :: Env -> FilePath
tmuxPath = shellTmux . sys

-- this is when I should probably start looking into lens...
setTmuxPath :: FilePath -> Env -> Env
setTmuxPath fp env = env { sys = (sys env) { shellTmux = fp } }

data TmuxCmdLine = TmuxCmdLine
  { c256        :: Any
  , controlMode :: Any
  , loginShell  :: Any
  , quiet       :: Any
  , utf8        :: Any
  , verbose     :: Any
  , shellCommand:: First String
  , altConfig   :: First String
  , socketName  :: First String
  , socketPath  :: First String
  , commands    :: Maybe [String]
  } deriving (Eq,Show,Generic)

instance Default TmuxCmdLine

getSocket :: TmuxCmdLine -> Text
getSocket tmuxCmdLine = T.pack . fromMaybe "" . NT.unpack
                      $ (socketPath tmuxCmdLine <> socketName tmuxCmdLine)

