{-# LANGUAGE OverloadedStrings #-}
{-# OPTIONS_GHC -fno-warn-orphans #-}

module Tmux.Util where

import Prelude hiding (takeWhile)
import Data.List as L hiding (takeWhile)
import Data.Char as Ch
import Data.Data
import Data.Generics
import Data.Int
import Data.Easy
import Data.Monoid
import Data.Text (Text)
import qualified Data.Text as T
import Data.Attoparsec.Text.Parsec as AT
import qualified Data.Map as Map
import Control.Applicative
import Control.Monad

import Tmux.Classes

import Language.Haskell.TH
import Language.Haskell.TH.Syntax

singleMapElement :: Map.Map k v -> (k,v)
singleMapElement mp = case Map.assocs mp of
  [] -> error "Unexpected empty map on onlyMapElement"
  ((k,v):[]) -> (k,v)
  _ -> error "Map provided to onlyMapElement has more than one element"


dropTillLast :: Char -> String -> String
dropTillLast c xs = let
  (ok,remain) = break (==c) xs
  in if null remain
    then ok
    else dropTillLast c (tail remain)

dropPrefix :: String -> String
dropPrefix = dropTillLast '.'

getTypeNames' :: VarStrictType -> [String]
getTypeNames' (nm,_,_) = [show nm]

getTypeTypes' :: VarStrictType -> [String]
getTypeTypes' (_,_,ConT tp) = [show tp]
getTypeTypes' _ = []

getTypeCount' :: VarStrictType -> Integer
getTypeCount' (_,_,ConT _) = 1
getTypeCount' _ = 0

getDataMemberTypes' :: (Typeable a, Data a) => a -> [String]
getDataMemberTypes' = everything (++) ([] `mkQ` getTypeNames')

getDataMemberTypes :: (Typeable a, Data a) => a -> ExpQ
getDataMemberTypes = stringE
                   . (" -F " ++)
                   . L.concatMap toTmuxFormat
                   . getDataMemberTypes'

toTmuxFormat :: String -> String
toTmuxFormat = fromMonoid "\t" . toTmuxF . dropPrefix
  where
    upperToUnder c = if isUpper c then ['_',Ch.toLower c] else [c]
    toTmuxF   = list "" (\tx' -> "\t#{" ++ concatMap upperToUnder tx' ++ "}")

getParser :: (Typeable a, Data a) => a -> [String]
getParser = map dropPrefix . everything (++) ([] `mkQ` getTypeTypes')

getCount :: (Typeable a, Data a) => a -> Integer
getCount = everything (+) (0 `mkQ` getTypeCount')


takeTillSep :: Parser Text
takeTillSep = try (takeWhile (/= '\t')) <|> fail "Does not end with separator"

sep :: Parser Char
sep = try (char '\t') <|> fail "Expected separator"

instance ParseTmux Int64 where
  parse' = sep >> try decimal <|> fail "Invalid Integer"

parseText :: (Text -> b) -> Parser b
parseText con = fmap con $ sep >> takeTillSep <|> fail "Invalid text"

parseTextSingleton :: (Text -> b) -> Parser [b]
parseTextSingleton con
  = fmap (singleton . con) $ sep >> try takeTillSep <|> fail "Invalid text"

parseNumPrefix :: Integral a => (a -> b) -> Char -> Parser b
parseNumPrefix con ch = fmap con $ try (sep >> char ch >> decimal) <|> fail "Invalid Number Prefix"

parseNum :: Integral a => (a -> b) -> Parser b
parseNum con = fmap con $ try (sep >> decimal) <|> fail "Invalid Number"

parseInt :: Parser Int
parseInt = sep >> decimal <|> fail "Invalid Number"

-- | Parse a boolea value (1 | 0)
-- ATTN: this function only really looks for a '1', and gladly accepts empty
-- text.
-- TODO: make this more robust
parseBool :: (Bool -> b) -> Parser b
parseBool con = fmap (\ab -> con (ab=='1')) $ sep >> char '1' <|> return '0'

{-# ANN parseCommand'' ("HLint: ignore Use notElem"::String) #-}
-- ATTN: does not properly handle two consecutive spaces
parseCommand'' :: Parser Text
parseCommand'' = do
  start <- takeWhile (\c -> c /= '\\' && c /= '"' && c /= ' ')
  sepr  <- try (char '\\') <|> try (char '"') <|> try (char ' ') <|> return ' '
  case sepr of

    '\\' -> do
              plusOne <- try (AT.take 1)
              return $ start <> T.singleton sepr <> plusOne

    '"'  -> do
              inQuotes <- try parseTillEndOfQuotes
              return $ start <> inQuotes
              {-return $ start <> (T.singleton sepr) <> inQuotes-}

    _   ->  if T.null start
              then fail $ "Empty result:" ++ T.unpack start
              else return start

parseCommand' :: Parser [Text]
parseCommand' = do
  comms <- many (try parseCommand'')
  nonEmpty <$> if Prelude.null comms
    then fmap (: []) takeText
    else do
      lastc <- takeText
      return $ comms ++ [lastc]


parseCommand :: Text -> [Text]
parseCommand cs = either (\x -> error $ "Invalid tmux command - cannot parse:" ++ x)
                         (fromMonoid $ error "Empty tmux command")
                       $ parseOnly parseCommand' cs

parseCommandEither :: Text -> Either String [Text]
parseCommandEither = parseOnly parseCommand'

parseTillEndOfQuotes :: Parser Text
parseTillEndOfQuotes = do
  start <- takeWhile (\c -> c /= '\\' && c /= '"')
  sepr  <- char '\\' <|> char '"'
  case sepr of

    '\\' -> do
              plusOne <- AT.take 1
              remain  <- parseTillEndOfQuotes
              return $ start <> T.singleton sepr <> plusOne <> remain

    _   -> do
      nc <- peekChar
      if nc == Just ' '
        then char ' ' >> return start
        else return start

isEnd :: Char -> Bool
isEnd c = (isSpace c && c /= ' ') || (c == ')') || (c == '\'')

parse :: Text -> Parser a -> Either String a
parse = flip parseOnly

(>>%) :: Monad m => m b -> m a -> m b
(>>%) = flip (>>)
infixl 1 >>%

parseTypeLoc :: Text -> Text -> Maybe Text
parseTypeLoc cmd res = either (const Nothing) Just $ parse res $ do
  _ <- string cmd >> space >> string "is" >> space
  try       (string "aliased to `" >> takeTill isEnd)
    <|> try (string "hashed (" >> takeTill isEnd)
    <|> takeTill isEnd

parseTmuxVersion :: Text -> Maybe (Int,Int)
parseTmuxVersion vers = either (const Nothing) Just $ parse vers $ do
  major <- string "tmux" >> space >> decimal
  minor <- char '.' >> decimal
  return (major, minor)

parseTmuxNumeric :: Text -> Text -> Int
parseTmuxNumeric desc txt = either (const 0) id $ parse txt $
  string desc >> skipSpace >> decimal
--
-- | @>>@ like but it keeps the result of the first operation, and uses its value
-- only for the side effects of the following operations
(<>>=) :: (Functor m, Monad m) => m a -> (a -> m b) -> m a;
x <>>= f = x >>= ap (<$) f;
infixl 1 <>>=;

-- | Infix fmap followed by function composition
(<$>.) :: Functor f => (a -> b) -> (c -> f a) -> c -> f b
funct <$>. fRetFunction = fmap funct . fRetFunction
infixl 4 <$>.

-- | Common starting words
samePrefixWords :: String -> String -> Bool
samePrefixWords refstr tststr = let
  ref = T.words . T.strip . T.pack $ refstr
  tst = T.words . T.strip . T.pack $ tststr
  in ref `isInfixOf` tst

prefixInWordList :: [String] -> String -> Bool
prefixInWordList reflst tststr = or $ fmap (`samePrefixWords` tststr) reflst


-- | Determine if string is a known shell
-- This function is a bit dumb, and will need frequent updates
isShell :: FilePath -> Bool
isShell = flip elem  [ "/bin/sh"
                 , "/bin/bash"
                 , "/bin/zsh"
                 , "/bin/fish"
                 , "/bin/ksh"
                 , "/bin/csh"
                 , "/bin/tcsh"
                 , "/bin/ash"
                 , "/bin/dash"
                 , "/bin/pdksh"
                 , "/bin/mksh"
                 , "/bin/psh"

                 , "/usr/bin/sh"
                 , "/usr/bin/bash"
                 , "/usr/bin/zsh"
                 , "/usr/bin/fish"
                 , "/usr/bin/ksh"
                 , "/usr/bin/csh"
                 , "/usr/bin/tcsh"
                 , "/usr/bin/ash"
                 , "/usr/bin/dash"
                 , "/usr/bin/pdksh"
                 , "/usr/bin/mksh"
                 , "/usr/bin/psh"
                 ]
