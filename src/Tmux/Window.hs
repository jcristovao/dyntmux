{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE DeriveDataTypeable #-}
{-# LANGUAGE FlexibleInstances #-}
{-# OPTIONS_GHC -fno-warn-orphans #-}

module Tmux.Window where

import Prelude hiding (takeWhile)
import Data.Int
import Data.Ord
import Data.Data
import Data.Easy
import qualified Data.IntMap as IM
import Data.Text as T hiding (takeWhile)
import Data.Text.Util
import Text.Printf
import Data.Attoparsec.Text.Parsec

import Control.Applicative
import Control.Monad.Trans.Reader

import Language.Haskell.TH

import Tmux.Types
import Tmux.Classes
import Tmux.Util
import Tmux.Pane
import Tmux.Layout

default (T.Text)

data WindowFlag
  = WCurrent
  | WLast
  | WActivity
  | WBell
  | WContent
  | WSilent
  | WZoomed
  | WNormal
  deriving (Eq,Ord,Typeable,Data,Show)

type WindowFlags =  [WindowFlag]

type    WindowIndex  = Int
newtype WindowName   = WName Text      deriving (Eq,Ord,Typeable,Data,Show)
newtype WindowId     = WID Int64       deriving (Eq,Ord,Typeable,Data)
type    WindowLayout = Layout
newtype WindowActive = WActive Bool    deriving (Eq,Ord,Typeable,Data,Show)

instance Show WindowId where
  show (WID sid) = '@':show sid

data Window = Window
  { windowName    :: WindowName
  , windowIndex   :: WindowIndex
  , windowId      :: WindowId
  , windowWidth   :: Int64
  , windowHeight  :: Int64
  , windowPanes   :: Int64
  , windowLayout  :: WindowLayout
  , windowFlags   :: WindowFlags
  , windowActive  :: WindowActive
  }
  deriving (Eq,Typeable)

instance Ord Window where compare = comparing windowId

instance Show Window where
  show w = printf "%s%s: [%s] (%ix%i) #%i %s | %s %s"
    (show . windowIndex $ w)
    (show . windowId    $ w)
    (show . windowName  $ w)
    (windowWidth  w)
    (windowHeight w)
    (windowPanes  w)
    (show . windowLayout $ w)
    (show . windowFlags  $ w)
    (show . windowActive $ w)

instance ToTmux Window where
  toTmux w = do
    t <- fmap (fromJustNote "No session passed to Window rendering")
       $ asks sessionName'
    -- default path is not reaching here
    startDir <- maybe "" (\dp -> " -c " ++. dp) <$> asks defaultPath
    return ("new-window -k"
            <> startDir
            <> " -n " <> windowN w
            <> " -t " <> t <> ":" ++. (show . windowIndex) w <> "\n"
            <> "set-window-option allow-rename off\n"
           , selLayout w
           )

windowN :: Window -> Text
windowN = (\(WName nm) -> nm) . windowName

selLayout :: Window -> Text
selLayout w = "select-layout " ++. (show . windowLayout) w <> "\n"

-- GHC 7.8 / TH 2.9
$(return [])

{-# ANN list_windows ("HLint: ignore Use camelCase"::String) #-}
list_windows :: Show a => a -> Text
list_windows s = T.pack $  "list-windows -t " ++ show s
                        ++ $(getDataMemberTypes =<< reify ''Window)

instance ParseTmux WindowName   where parse' = parseText WName
instance ParseTmux WindowIndex  where parse' = parseInt
instance ParseTmux WindowId     where parse' = parseNumPrefix WID '@'
instance ParseTmux WindowLayout where parse' = sep >> parseL
instance ParseTmux WindowActive where parse' = parseBool WActive

instance ParseTmux WindowFlag where
  parse'
    =   (char '*' >> return WCurrent)
    <|> (char '-' >> return WLast)
    <|> (char '#' >> return WActivity)
    <|> (char '!' >> return WBell)
    <|> (char '+' >> return WContent)
    <|> (char '~' >> return WSilent)
    <|> (char 'Z' >> return WZoomed)
    <|> (char ' ' >> return WNormal)
    <|> fail "Invalid Window Flag"

instance ParseTmux WindowFlags where parse' = sep >> many1' parse'

type WindowPanes = IM.IntMap (Window,[Pane])

instance ToTmux WindowPanes where
  toTmux wps = do
    env <- ask
    let toTmux' w = runRdr env $ toTmux w
        totmux (w,ps) acc
          =   fst (toTmux' w)
          <> (fst . toTmux' . windowLayout $ w)
          <> (T.concat . Prelude.map (fst . toTmux') $ ps)
          <> snd (toTmux' w)
          <> acc
    return (IM.foldr' totmux "" wps,T.empty)


