{-# LANGUAGE OverloadedStrings #-}

module Tmux.CmdLine
  ( MainOpts(..)
  , mainOpts
  , MainCommand(..)
  , TmuxinatorOpts(..)
  , LoadOpts(..)
  , loadOptsToProjLocation
  , SaveOpts(..)
  , saveOptsToProjLocation
  , ListOpts(..)
  , listOptsToProjLocation
  , EditOpts(..)
  , editOptsToProjLocation
  , TmuxCmdLine(..)
  , parseTmuxCmdLine
  , parseTmuxCmdLine'
  {-, usageS-}
  {-, usageT-}
  , confirm
  , okS, koS, noS
  , okT, koT, noT
  ) where

import Data.Monoid
import qualified Data.Char as Ch
import Options.Applicative
import System.Console.CmdArgs.Explicit
import System.Console.ANSI
import Data.Text (Text)
import Data.Text.Util

import Control.Newtype

import Tmux.Types
import Tmux.Folders

data MainOpts = MainOpts
  { optVersion    :: Bool
  , optDebug      :: Bool
  , optColour     :: Bool
  , optTmux       :: Maybe TmuxCmdLine
  , optCommand    :: Maybe MainCommand
  } deriving (Eq,Show)

data MainCommand
  = Nator TmuxinatorOpts
  | Load  LoadOpts
  | Save  SaveOpts
  | List  ListOpts
  | Edit  EditOpts
  | Doctor
  deriving (Eq,Ord,Show)

parseMainOpts :: Parser MainOpts
parseMainOpts = MainOpts
  <$> switch (long "version"
              <> help "print dyntmux version")
  <*> switch (short 'd' <> long "debug"
              <> help "print tmux commands instead of executing them")
  <*> switch (short 'n' <> long "no-colour"
              <> help "disable colour output")
  <*> optional parserTmuxCmdLine'
  <*> optional (subparser
    (  command "tmuxinator" (info parseTmuxinatorOpts
                           (progDesc "Tmuxinator compatibility"))
    <> command "load" (info parseLoadOpts
                       (progDesc "Load a project into a tmux session"))
    <> command "save" (info parseSaveOpts
                       (progDesc "Save projects"))
    <> command "list" (info parseListOpts
                       (progDesc "List projects"))
    <> command "edit" (info parseEditOpts
                       (progDesc "Edit Project"))
    <> command "open" (info parseEditOpts
                       (progDesc "Edit Project"))
    <> command "doctor" (info (pure Doctor)
                       (progDesc "Checks your configuration"))
    ))

mainOpts :: ParserInfo MainOpts
mainOpts = info (helper <*> parseMainOpts)
  ( fullDesc
  <> progDesc "Dynamic control of tmux"
  <> header ("\nSave and restore tmux sessions, windows and panes into projects.\n"
           <>"\nUndocumented options are tmux options, and take precedence over "
           <>  "project options.\n"))


{-usageS :: String -> String-}
{-usageS = usage (prefs showHelpOnError) parseMainOpts-}

{-usageT :: Text -> Text-}
{-usageT = T.pack . usageS . T.unpack-}

data TmuxinatorOpts = TmuxinatorOpts
  { natorList    :: Bool
  , natorImplode :: Bool
  } deriving (Eq,Ord,Show)

parseTmuxinatorOpts :: Parser MainCommand
parseTmuxinatorOpts = Nator <$> (TmuxinatorOpts
  <$> switch (short 'l' <> long "list"
              <> help "list all configured tmuxinator projects")
  <*> switch (long "rf" <> long "implode"
              <> help "remove all tmuxinator configs, aliases and scripts")
  )

data LoadOpts = LoadOpts
  { loadFromNator  :: Bool
  , loadIntellig   :: Bool
  , loadGConfig    :: Bool
  , loadFromCurr   :: Bool
  , loadProj       :: Maybe String
  } deriving (Eq,Ord,Show)

-- | Common project options for load, edit == open
commonProjOpts
  :: (a -> b)
  -> (Bool -> Bool -> Bool -> Bool -> Maybe String -> a)
  -> String
  -> Parser b
commonProjOpts c opts filedesc = c <$> (opts
  <$> switch (short 'o' <> long "from-tmuxinator"
              <> help "from tmuxinator projects")
  <*> switch (short 's' <> long "smart-location"
              <> help "from git if in a repository, else from configuration directory")
  <*> switch (short 'g' <> long "from-config"
              <> help "from dyntmux configuration directory (~/.config/dyntmux)")
  <*> switch (short 'c' <> long "from-cwd"
              <> help "from current working directory")
  <*> optional ( argument str ( metavar "PROJECT"
              <> help filedesc))
  )

parseLoadOpts :: Parser MainCommand
parseLoadOpts =
  commonProjOpts Load LoadOpts "project to load (or attach if it is already loaded)."

loadOptsToProjLocation :: LoadOpts -> ProjLocation
loadOptsToProjLocation opts
  | loadFromNator opts = LocatNator
  | loadGConfig   opts = LocatConfig
  | loadFromCurr  opts = LocatCurr
  | otherwise          = LocatSmart

data SaveOpts = SaveOpts
  { saveToNator  :: Bool
  , saveIntellig :: Bool
  , saveGConfig  :: Bool
  , saveToCurr   :: Bool
  , saveBase     :: Maybe String
  , saveProjName :: Maybe String
  } deriving (Eq,Ord,Show)


parseSaveOpts :: Parser MainCommand
parseSaveOpts = Save <$> (SaveOpts
  <$> switch (short 'o' <> long "to-tmuxinator"
              <> help "save in tmuxinator projects (not all features supported)")
  <*> switch (short 's' <> long "smart-location"
              <> help "save in git repository if available, default configuration directory otherwise")
  <*> switch (short 'g' <> long "to-config"
              <> help "to dyntmux configuration directory (~/.config/dyntmux)")
  <*> switch (short 'c' <> long "to-cwd"
              <> help "to current working directory")
  <*> optional (strOption (short 'b' <> long "base"
              <> help "project base directory (will use current git repository or CWD if omitted"))
  <*> optional ( argument str ( metavar "PROJECT"
              <> help "save current tmux session to a project."))
  )

saveOptsToProjLocation :: SaveOpts -> ProjLocation
saveOptsToProjLocation opts
  | saveToNator opts = LocatNator
  | saveGConfig opts = LocatConfig
  | saveToCurr opts  = LocatCurr
  | otherwise        = LocatSmart

data ListOpts = ListOpts
  { listFromNator  :: Bool
  , listIntellig   :: Bool
  , listGConfig    :: Bool
  , listFromCurr   :: Bool
  } deriving (Eq,Ord,Show)

listOptsToProjLocation :: ListOpts -> ProjLocation
listOptsToProjLocation opts
  | listFromNator opts = LocatNator
  | listIntellig  opts = LocatSmart
  | listGConfig   opts = LocatConfig
  | listFromCurr  opts = LocatCurr
  | otherwise          = LocatSmart

parseListOpts :: Parser MainCommand
parseListOpts = List <$> (ListOpts
  <$> switch (short 'o' <> long "from-tmuxinator"
              <> help "from tmuxinator projects")
  <*> switch (short 's' <> long "smart-location"
              <> help "from git if in a repository, else from configuration directory")
  <*> switch (short 'g' <> long "from-config"
              <> help "from dyntmux configuration directory (~/.config/dyntmux)")
  <*> switch (short 'c' <> long "from-cwd"
              <> help "from current working directory")
  )

data EditOpts = EditOpts
  { editFromNator  :: Bool
  , editIntellig   :: Bool
  , editGConfig    :: Bool
  , editFromCurr   :: Bool
  , editProj       :: Maybe String
  } deriving (Eq,Ord,Show)

parseEditOpts :: Parser MainCommand
parseEditOpts =
  commonProjOpts Edit EditOpts "project to edit."

editOptsToProjLocation :: EditOpts -> ProjLocation
editOptsToProjLocation opts
  | editFromNator opts = LocatNator
  | editGConfig   opts = LocatConfig
  | editFromCurr  opts = LocatCurr
  | otherwise          = LocatSmart



parserTmuxCmdLine :: Parser TmuxCmdLine
parserTmuxCmdLine = TmuxCmdLine
  <$> fmap pack (switch (short '2'))
  <*> fmap pack (switch (short 'C' <> help "Start in control mode"))
  <*> fmap pack (switch (short 'l'))
  <*> fmap pack (switch (short 'q'))
  <*> fmap pack (switch (short 'u'))
  <*> fmap pack (switch (short 'v'))
  <*> fmap pack (optional (strOption (short 'c' <> metavar "shell-command")))
  <*> fmap pack (optional (strOption (short 'f' <> metavar "configuration-file")))
  <*> fmap pack (optional (strOption (short 'L' <> metavar "socket-name")))
  <*> fmap pack (optional (strOption (short 'S' <> metavar "socket-path")))
  <*> optional (some (argument str (metavar "commands...")))

parseTmuxCmdLine :: String -> Either ParserFailure TmuxCmdLine
parseTmuxCmdLine tmuxCmdLine =
  case execParserPure (prefs showHelpOnError)
                      (info parserTmuxCmdLine briefDesc)
                      (splitArgs' tmuxCmdLine)
  of
  Success a -> Right a
  Failure f -> Left f
  CompletionInvoked _ -> error "this should not happen"


-- | Tmux command line to be mixed with tmux options
parserTmuxCmdLine' :: Parser TmuxCmdLine
parserTmuxCmdLine' = TmuxCmdLine
  <$> fmap pack (switch (short '2'))
  <*> pure (pack False)
  <*> pure (pack False)
  <*> fmap pack (switch (short 'q'))
  <*> fmap pack (switch (short 'u'))
  <*> fmap pack (switch (short 'v'))
  <*> pure (pack Nothing)
  <*> fmap pack (optional (strOption (short 'f' <> metavar "configuration-file")))
  <*> fmap pack (optional (strOption (short 'L' <> metavar "socket-name")))
  <*> fmap pack (optional (strOption (short 'S' <> metavar "socket-path")))
  <*> pure Nothing

parseTmuxCmdLine' :: String -> Either ParserFailure TmuxCmdLine
parseTmuxCmdLine' tmuxCmdLine =
  case execParserPure (prefs showHelpOnError)
                      (info parserTmuxCmdLine' briefDesc)
                      (splitArgs' tmuxCmdLine)
  of
  Success a -> Right a
  Failure f -> Left f
  CompletionInvoked _ -> error "this should not happen"

data LastPost = Space | Other | Dash | FirstLetter deriving (Eq)

-- Splits "-abc" into ["-a","-b","-c"]
-- Ignores "abc-acb"
-- skips multi word parameters
-- TODO: merges "-a-b" into "-ab"
-- TODO FIX: uses a left fold, lists, and it is just plain ugly
splitArgs' :: String -> [String]
splitArgs' = let
  applySingle f s = if (length . words $ s) <= 1
                      then f s
                      else [s]
  addChar [] c = [[c]]
  addChar (lst:_)  c = [lst ++ [c]]
  splitDashes = fst . foldl (\acc c -> case c of
          '-' -> case snd acc of
            Space -> (addChar (fst acc) c, Dash)
            Dash  -> (addChar (fst acc) c, Other)
            Other -> (addChar (fst acc) c, Other)
            FirstLetter -> (fst acc ++ addChar [] c, Dash)
          ' ' -> (addChar (fst acc) c, Space)
          _   -> case snd acc of
            Dash -> (addChar (fst acc) c, FirstLetter)
            FirstLetter -> (fst acc ++ [['-',c]],FirstLetter)
            Space -> (addChar (fst acc) c, Other)
            Other -> (addChar (fst acc) c, Other)
          ) ([""],Space)
  in concatMap (applySingle splitDashes) . splitArgs

confirm :: IO Bool
confirm = do
  let syntax = Prelude.putStrLn "Please indicate [y]es or [n]o:"
  res <- Prelude.getLine
  case res of
    "" -> do
      syntax
      confirm
    (x:_) | Ch.toUpper x == 'Y' -> return True
          | Ch.toUpper x == 'N' -> return False
          | otherwise -> do
                syntax
                confirm

okS, koS, noS :: String -> String
okS = (setSGRCode [SetColor Foreground Vivid Green] <>)
koS = (setSGRCode [SetColor Foreground Vivid Red  ] <>)
noS = (setSGRCode [Reset                          ] <>)

okT, koT, noT :: Text -> Text
okT = (setSGRCode [SetColor Foreground Vivid Green] .++)
koT = (setSGRCode [SetColor Foreground Vivid Red  ] .++)
noT = (setSGRCode [Reset                          ] .++)

