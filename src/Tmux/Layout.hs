{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE DeriveDataTypeable #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE OverlappingInstances #-}
{-# LANGUAGE RankNTypes #-}
{-# OPTIONS_GHC -fno-warn-orphans #-}
module Tmux.Layout where

import Prelude hiding (takeWhile)
import Safe
import Data.Int
import Data.Bits
import Data.Word
import Data.Data
import Data.Monoid
import Data.List as L
import Data.Text as T hiding (takeWhile)
import Text.Printf
import Data.Text.Format
import Data.Text.Util
import Data.Text.Encoding
import qualified Data.ByteString as BS
import Data.Attoparsec.Text.Parsec

import Control.Applicative
import Control.Monad
import Control.Monad.Trans.State

import Tmux.Classes
import Tmux.Types
import Tmux.Pane

default (T.Text)

paneMinimum :: Int64
paneMinimum = 2

-- vertical and horizontal split characters (Begin and End)
vscB,vscE,hscB,hscE :: Char
vscB = '{'
vscE = '}'
hscB = '['
hscE = ']'

data LayoutContents
  = LCPaneId PaneId
  | LCVertSplit [LayoutUnit]
  | LCHorzSplit [LayoutUnit]
  deriving (Eq,Ord,Typeable)

instance ParseLayout LayoutContents where
  parseL =  try parseHorizSplit
        <|> try parseVertSplit
        <|> try (LCPaneId . PID <$> (char ',' *> decimal))

parseHorizSplit :: Parser LayoutContents
parseHorizSplit = do
  void $ char hscB
  lus <- parseL
  void $ char hscE
  return $ LCHorzSplit lus

parseVertSplit :: Parser LayoutContents
parseVertSplit = do
  void $ char vscB
  lus <- parseL
  void $ char vscE
  return $ LCVertSplit lus

instance Show LayoutContents where
  show lc = case lc of
    LCPaneId pid    -> ','  : (show . (\(PID p) -> p) $ pid)
    LCVertSplit lus -> vscB : showUnits lus ++ [vscE]
    LCHorzSplit lus -> hscB : showUnits lus ++ [hscE]

data LayoutUnit = LayoutUnit
  { luWidth   :: Int64
  , luHeight  :: Int64
  , luX0      :: Int64
  , luY0      :: Int64
  , luContents:: LayoutContents
  } deriving (Eq,Ord,Typeable)

instance ParseLayout LayoutUnit where
  parseL
    =  LayoutUnit
   <$> (parseL <* char 'x') -- w
   <*> (parseL <* char ',') -- h
   <*> (parseL <* char ',') -- x0
   <*> parseL               -- y0
   <*> parseL               -- cs

instance Show LayoutUnit where
  show lu =  (show . luWidth $ lu) ++ "x" ++ (show . luHeight $ lu) ++ ","
          ++ (show . luX0 $ lu) ++ "," ++ (show . luY0 $ lu)
          ++ (show . luContents $ lu)

type LayoutUnits = [LayoutUnit]

instance ParseLayout LayoutUnits where parseL = parseL `sepBy1` char ','

showUnits :: [LayoutUnit] -> String
showUnits lus = L.intercalate "," $ L.map show lus

instance Show LayoutUnits where show = showUnits

data Layout = Layout
  { checkSum    :: Int64
  , layoutType  :: LayoutType
  , layoutUnits :: [LayoutUnit]
  } deriving (Eq,Ord,Typeable)

defaultLayout :: Layout
defaultLayout = Layout 0 LayoutTiled []

instance Show Layout where
  show (Layout csum LayoutCustom unts)
    =  printf "%04x" csum ++ "," ++ showUnits unts
  show (Layout _ tp _) = show tp

instance ToTmux Layout where
  toTmux layout = return (renderLayout layout,T.empty)

instance ParseLayout Layout where
  parseL
    =  Layout <$> try hexadecimal
              <*> return LayoutCustom
              <*> try (char ',' *> parseL)
   <|> try (fixedLayout "even-horizontal" LayoutEvenHorizontal)
   <|> try (fixedLayout "even-vertical"   LayoutEvenVertical)
   <|> try (fixedLayout "main-horizontal" LayoutMainHorizontal)
   <|> try (fixedLayout "main-vertical"   LayoutMainVertical)
   <|> try (fixedLayout "tiled"           LayoutTiled)
    where
      fixedLayout str lay = Layout <$> return 0
                                    <*> (string str >> return lay)
                                    <*> return []

data LayoutType
  = LayoutCustom
  | LayoutEvenHorizontal
  | LayoutEvenVertical
  | LayoutMainHorizontal
  | LayoutMainVertical
  | LayoutTiled
  deriving (Eq,Ord,Typeable,Data)

instance Show LayoutType where
  show LayoutEvenHorizontal = "even-horizontal"
  show LayoutEvenVertical   = "even-vertical"
  show LayoutMainHorizontal = "main-horizontal"
  show LayoutMainVertical   = "main-vertical"
  show LayoutTiled          = "tiled"
  show _                    = error "INVALID LAYOUT"

instance ParseLayout Int64 where
  parseL = decimal <|> fail "Invalid integer in layout"

data CurrSplit = SplitHorz Int64 | SplitVert Int64 deriving (Eq,Show)
type Splits = (CurrSplit,[CurrSplit])

defaultSplits :: Splits
defaultSplits = (SplitHorz 0, [])

renderLayout :: Layout -> Text
renderLayout layout = evalState (renderLayout1 layout) defaultSplits

renderLayout1 :: Layout -> State Splits Text
renderLayout1 layout = case layoutUnits layout of
  []     -> return T.empty
  (x:[]) -> case luContents x of
    LCPaneId _      -> return T.empty
    LCVertSplit lus -> do
      put (SplitVert $ luWidth x,[])
      renderLayout' lus
    LCHorzSplit lus -> do
      put (SplitHorz $ luHeight x,[])
      renderLayout' lus
  (_:_) -> error "Provided layout cannot be rendered"

splitWindow :: LayoutUnit -> Bool -> Splits -> Text
splitWindow lu same st = let
  command = if same then "split-window -d " else "split-window "
  in case fst st of
      SplitHorz l -> command <> "-vl " <> toText (l - luHeight lu) <> "\n"
      SplitVert l -> command <> "-hl " <> toText (l - luWidth  lu) <> "\n"

shft :: LayoutUnit -> Splits -> Splits
shft _ (_,[]) = error "Unexpected condition when moving up in layout stack"
shft lu (_,SplitHorz l:sx) = (SplitHorz (l - luHeight lu),sx)
shft lu (_,SplitVert l:sx) = (SplitVert (l - luWidth  lu),sx)

renderLayout' :: [LayoutUnit] -> State Splits Text
renderLayout' [] = error "Invalid empty list in layout"
renderLayout' (x:[]) = case luContents x of
  LCPaneId _  -> do
    how <- fmap fst get
    let curr = case how of
          SplitHorz _ -> "select-pane -R\n"
          SplitVert _ -> "select-pane -D\n"
    modify $ shft x
    return curr

  LCVertSplit lus -> do
    modify $ \(c,lst) -> (SplitVert $ luWidth x, c:lst)
    res <- renderLayout' lus
    modify $ shft x
    return res

  LCHorzSplit lus -> do
    modify $ \(c,lst) -> (SplitHorz $ luHeight x, c:lst)
    res <- renderLayout' lus
    modify $ shft x
    return res

renderLayout' (x:xs) = case luContents x of
  LCPaneId _  -> do
    curr <- fmap (splitWindow x False) get
    let sub (SplitHorz l,lst) = (SplitHorz (l - luHeight x),lst)
        sub (SplitVert l,lst) = (SplitVert (l - luWidth  x),lst)
    modify sub
    fmap (T.append curr) $ renderLayout' xs

  LCVertSplit lus -> do
    curr <- fmap (splitWindow x True) get
    modify $ \(c,lst) -> (SplitVert $ luWidth x, c:lst)
    subRes <- fmap (T.append curr) $ renderLayout' lus
    fmap (T.append subRes) $ renderLayout' xs

  LCHorzSplit lus -> do
    curr <- fmap (splitWindow x True) get
    modify $ \(c,lst) -> (SplitHorz $ luHeight x, c:lst)
    subRes <- fmap (T.append curr) $ renderLayout' lus
    fmap (T.append subRes) $ renderLayout' xs

toText :: Int64 -> Text
toText x = l2s $ format "{}" $ Only . toInt $ x
  where toInt :: Int64 -> Int
        toInt = fromIntegral

layoutCheckSum :: Text -> Word16
layoutCheckSum layout = let
  laybs = encodeUtf8 $ T.drop 5 layout
  checksum csum x =  (csum `shift` (-1))
                   + ((csum .&. 1) `shift` 15)
                   + fromIntegral x
  in BS.foldl' checksum 0 laybs

checkLayout :: Text -> Bool
checkLayout layout = let
  originalCSum :: Int64
  originalCSum = either (const (-1)) id $ parseOnly hexadecimal $ T.take 4 layout
  calculatedCSum = layoutCheckSum layout
  in originalCSum == fromIntegral calculatedCSum

maxDimensions :: Layout -> Maybe ScreenSize
maxDimensions layout = let
  sss = filterDimensions layout
  in case sss of
    [] -> Nothing
    xs -> Just . L.maximum $ xs

filterDimensions :: Layout -> [ScreenSize]
filterDimensions layout
  | isCustom  = filterDimLUS . layoutUnits $ layout
  | otherwise = []
  where
    isCustom = layoutType layout == LayoutCustom
    filterDimLUS :: [LayoutUnit] -> [ScreenSize]
    filterDimLUS [] = []
    filterDimLUS (x:xs) = (luWidth x, luHeight x):(filterDimLC . luContents $ x)
                        ++ filterDimLUS xs
    filterDimLC :: LayoutContents -> [ScreenSize]
    filterDimLC (LCPaneId _) = []
    filterDimLC (LCVertSplit lus) = filterDimLUS lus
    filterDimLC (LCHorzSplit lus) = filterDimLUS lus

layoutToList :: Layout -> [(Int64,Int64,Int64,Int64)] -- w,h,x,y
layoutToList = join . L.map extDims . layoutUnits
  where
    extDims :: LayoutUnit -> [(Int64,Int64,Int64,Int64)]
    extDims lu = case luContents lu of
      LCPaneId _      -> [(luWidth lu, luHeight lu, luX0 lu, luY0 lu)]
      LCVertSplit lus -> join $ L.map extDims lus
      LCHorzSplit lus -> join $ L.map extDims lus


third :: (t, t1, t2) -> t2
third (_,_,t) = t

fillLayoutUnits' :: Layout -> Int64 -> ScreenSize -> Layout
fillLayoutUnits' l pns (x,y) = fillLayoutUnits l pns 0 (x,y)

-- Layout -> # Panes -> offset -> ScreenSize -> layout
fillLayoutUnits :: Layout -> Int64 -> Int64 -> ScreenSize -> Layout

fillLayoutUnits l@(Layout _ LayoutCustom _) _ _ _ = l

fillLayoutUnits (Layout cs LayoutEvenHorizontal _) pns off (x,y)
  = let sps= pns - 1    -- separators
        xd = (x - sps) `div` pns
        xs = x - sps - xd * sps
        slu= LayoutUnit xs y (xd * sps + sps) off (LCPaneId (PID (pns-1)))
        lu 0 acc = acc
        lu i (x',id',lus')
          = lu (i-1) ( x' - xd - 1
                     , id' - 1
                     , LayoutUnit xd y x' off (LCPaneId (PID id')):lus')
        lus = third $ lu (pns - 1) (xd * (sps - 1) + (sps - 1),pns-2,[slu])
    in Layout cs LayoutEvenHorizontal [LayoutUnit x y 0 off (LCVertSplit lus)]

fillLayoutUnits (Layout cs LayoutEvenVertical _) pns off (x,y)
  = let sps= pns - 1 --separators
        yd = (y - sps) `div` pns
        ys = y - sps - yd * sps
        slu= LayoutUnit x ys off (yd * sps + sps) (LCPaneId (PID (pns-1)))
        lu 0 acc = acc
        lu i (y',id',lus')
          = lu (i-1) ( y' - yd - 1
                     , id' - 1
                     , LayoutUnit x yd off y' (LCPaneId (PID id')):lus')
        lus = third $ lu (pns - 1) (yd * (sps - 1) + (sps - 1),pns-2,[slu])
    in Layout cs LayoutEvenVertical [LayoutUnit x y off 0 (LCHorzSplit lus)]

fillLayoutUnits (Layout cs LayoutMainHorizontal _) pns _ (x,y)
  = let ym = y `div` 3
        ym'= y - ym * 2
        lus'= fillLayoutUnits
                (Layout cs LayoutEvenHorizontal []) (pns-1) (ym'+2) (x,y-ym-2)
        lum = LayoutUnit x (ym'+1) 0 0 (LCPaneId (PID 0))
        luot= if pns > 2
                then
                  headNote "[ERROR] fillLayoutUnits MainHorizontal" . layoutUnits $ lus'
                else
                  LayoutUnit x (y-ym-2) 0 (ym'+2) (LCPaneId (PID 0))
        lus = [LayoutUnit x y 0 0 (LCHorzSplit [lum,luot])]
    in Layout cs LayoutMainHorizontal lus

fillLayoutUnits (Layout cs LayoutMainVertical _) pns _ (x,y)
  = let xm = x - xm'
        xm'= 79
        lus'= fillLayoutUnits
                (Layout cs LayoutEvenVertical []) (pns-1) (xm'+2) (xm-2,y)
        lum = LayoutUnit (xm'+1) y 0 0 (LCPaneId (PID 0))
        luot= if pns > 2
                then
                  headNote "[ERROR] fillLayoutUnits EvenVertical" . layoutUnits $ lus'
                else
                  LayoutUnit (xm-2) y (xm'+2) 0 (LCPaneId (PID 0))
        lus = [LayoutUnit x y 0 0 (LCVertSplit [lum,luot])]
    in Layout cs LayoutMainVertical lus

fillLayoutUnits (Layout cs LayoutTiled _) pns off (x,y) = let
    -- matrix with given rows and cols
    (rows,cols) = calc pns 1 1
    -- last row should only have colsl columns
    colsl = pns - ((rows - 1) * cols)
    -- cell delta (size)
    xd = let xd' = (x - (cols - 1)) `div` cols
          in if xd' < paneMinimum then paneMinimum else xd'
    yd = let yd' = (y - (rows - 1)) `div` rows
          in if yd' < paneMinimum then paneMinimum else yd'
    -- buggy new dimensions
    x' = (xd + 1) * cols - 1
    y' = (yd + 1) * rows - 1
    -- number of separators
    xsep = cols - 1
    ysep = rows - 1
    -- last cell on each row size (except perhaps the last row)
    xl = x - (xd + 1) * xsep
    -- last cell on last row size
    xll= x - (colsl - 1) * (xd + 1)
    -- last row height : hum? why not y'? tmux code is buggy...
    yl = y  - (yd + 1) * ysep
    -- regular line columns
    lineCols yoff cols' xd' yd' incLast = let
      fsts= L.map (\i -> LayoutUnit xd' yd' ((xd' + 1) * i) ((yd + 1) * yoff)
                   $ LCPaneId (PID (yoff * cols + i))) [0..(cols'-2)]
      lst = [ LayoutUnit xl yd' ((xd' + 1) * (cols' - 1)) ((yd + 1) * yoff)
            $ LCPaneId (PID (yoff * cols + (cols' - 1))) ]
      in if incLast
        then fsts ++ lst
        else fsts
    -- last line columns (only if >= 2)
    lastLineCols = lineCols (rows - 1) colsl xd yl False
                 ++[LayoutUnit xll yl (off + (colsl - 1) * (xd + 1)) ((rows - 1) * (yd + 1))
                    $ LCPaneId (PID pns)]
    luLines r
      | r > rows  = []
      | otherwise = let
        lr r'
          -- last line
          | r' == rows
            = if pns - ((r' - 1) * cols) == 1
                then
                  LayoutUnit xll yl off ((r' - 1) * (yd + 1))
                  $ LCPaneId (PID pns)
                else
                  LayoutUnit x  yl off ((r' - 1) * (yd + 1))
                  $ LCVertSplit lastLineCols
          -- not last line, but single column (1x1)
          | cols == 1
            = LayoutUnit x yd off ((r' - 1) * (yd + 1)) $ LCPaneId (PID 1)
          -- other lines besides last
          | otherwise
            = LayoutUnit x yd off ((r' - 1) * (yd + 1))
              $ LCVertSplit (lineCols (r' - 1) cols xd yd True)
        in lr r:luLines (r + 1)
  in Layout cs LayoutTiled [LayoutUnit x' y' 0 0 (LCHorzSplit (luLines 1))]

calc :: Int64 -> Int64 -> Int64 -> (Int64,Int64)
calc panes rows columns
  = if rows * columns < panes
      then calc panes (rows + 1)
            (if (rows + 1) * columns < panes then columns + 1 else columns)
      else (rows,columns)



