-- | Project format indepenend rules
module Tmux.Project.Rules where

import Data.Maybe
import Data.Easy
import Data.Text (Text)
import qualified Data.Text as T
import qualified Data.Char as Ch
import qualified Data.List as L
import qualified Data.Foldable as F
import Text.Printf

{-import qualified Tmux.Window as W-}
import qualified Tmux.Pane   as P

import System.FilePath
import System.Console.ANSI

import Control.Newtype
import Tmux.Types
import Tmux.Util

-- | Choose a project filename.
-- Optional project name (provided by command line) has priority,
-- otherwise provided project name (derived from session name)
-- will be used
projFilename
  :: FilePath       -- ^ Project Location (directory)
  -> Maybe FilePath -- ^ Optional Project filename (command line)
  -> Text           -- ^ Project name (session name)
  -> FilePath       -- ^ Returning project (full) filenames
projFilename projloc cmdLineName sessionName =
  case cmdLineName of
    Nothing -> projloc </> T.unpack sessionName
    Just fn -> projloc </> fn
  {-in replaceExtension projectName ".yml"-}


getProjectFormatStrict :: FilePath -> Maybe ProjectFormat
getProjectFormatStrict fp
  | normFe fp `elem` [".YML",".YAML",".TMUXINATOR"] = Just YamlTmuxinator
  | normFe fp `elem` [".DYNTMUX",".DYN",".DYNPROJ"] = Just YamlDynTmux
  | otherwise                                       = Nothing
  where normFe = fmap Ch.toUpper . takeExtension

-- | Defaults to YamlDynTmux format
getProjectFormat :: FilePath -> ProjectFormat
getProjectFormat = fromMaybe YamlDynTmux . getProjectFormatStrict

-- | Add a file extension based on requested project format
-- Silently discards incoherent existing extension (TODO: review this?),
-- while keeping (and just adding to) irrelevant extensions.
projAddFileExtension :: ProjectFormat -> FilePath -> FilePath
projAddFileExtension format fp = case format of
  YamlTmuxinator -> case getProjectFormatStrict fp of
    Nothing             -> addExtension fp ".yml"
    Just YamlTmuxinator -> fp
    Just YamlDynTmux    -> replaceExtension fp ".yml"
  YamlDynTmux    -> case getProjectFormatStrict fp of
    Nothing             -> addExtension fp ".dyn"
    Just YamlDynTmux    -> fp
    Just YamlTmuxinator -> replaceExtension fp ".dyn"

shouldChDir
  :: P.Pane         -- ^ Pane to evaluate
  -> Maybe FilePath -- ^ default path (if defined)
  -> FilePath       -- ^ Current path
  -> Bool           -- ^ should issue change directory command
shouldChDir pn defPath curPath = let
  panePath = T.unpack . unpack $ P.paneCurrentPath pn
  basePath = fromMaybe curPath defPath
  in not $ equalFilePath panePath basePath

getDirToGo
  :: P.Pane         -- ^ Pane to evaluate
  -> Maybe FilePath -- ^ default path (if defined)
  -> FilePath       -- ^ Current path
  -> [FilePath]     -- ^ List of possible files in command
  -> FilePath       -- ^ change directory params
getDirToGo pn defPath curPath otherFiles = let
  panePath = T.unpack . unpack $ P.paneCurrentPath pn
  basePath = fromMaybe curPath defPath
  relative = makeRelative basePath panePath
  firstGues= equalFilePath panePath relative ? panePath $ relative
  in pathGuess firstGues otherFiles

evalProjectStatus :: Bool -> (FilePath, Either Text a) -> String
evalProjectStatus colour (fn,stat) =
  case stat of
    Left err -> printf (setSGRCode red ++ "%-20s %s") fn $ T.unpack err
    Right _  -> printf (setSGRCode gre ++ "%-20s [OK]") fn
  where red = [ SetColor Foreground Vivid Red   | colour ]
        gre = [ SetColor Foreground Vivid Green | colour ]


-- | Ok, a bit of magic here, might be more trouble than its
-- worth.
pathGuess :: FilePath -> [FilePath] -> FilePath
pathGuess fp lfps = let
  fpParts = splitDirectories fp
  res     = foldl (\acc fp' -> pathUnShare acc (splitDirectories fp'))
              fpParts lfps
  in joinPath res

pathUnShare :: [FilePath] -> [FilePath] -> [FilePath]
pathUnShare dir file = let
  lfn  = length file
  ldr  = length dir
  indx = L.find (\i -> take i file `L.isSuffixOf` dir) . reverse $ [1..lfn]
  in case indx of
    Nothing -> dir
    Just j  -> take (ldr - j) dir

-- given a list of commands, extrapolate wildly to gather a list of files
-- passed to those commands
guessFilesFromCommands :: [FilePath] -> [FilePath]
guessFilesFromCommands = F.foldMap f
  where
    f = filter mayBeFilePath
      . either (const []) (fmap T.unpack)
      . parseCommandEither . T.pack


mayBeFilePath :: FilePath -> Bool
mayBeFilePath = atLeast 2 . splitDirectories
{-guessFilesFromCommands coms = let-}
  {-foldr (\c acc -> -}




