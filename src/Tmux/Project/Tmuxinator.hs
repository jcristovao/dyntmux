{-# LANGUAGE OverloadedStrings    #-}
{-# LANGUAGE DeriveGeneric        #-}
{-# LANGUAGE ScopedTypeVariables  #-}
{-# OPTIONS_GHC -fno-warn-orphans #-}

module Tmux.Project.Tmuxinator where

import Data.Easy
import Data.Monoid
import Data.Text (Text)
import qualified Data.Text as T
import Data.Text.IO as T
import Data.Text.Util
import qualified Data.List as L
import Data.Text.Encoding
import qualified Data.ByteString as BS

import Data.Yaml
import YamlError
{-import Data.Aeson.Types (typeMismatch)-}
import Data.Aeson (genericToJSON)
import Data.Aeson.Types (Options(..),defaultOptions)
import Data.Default.Generics
import qualified Data.Attoparsec.Text.Parsec as Atto

import Control.Applicative
import Control.Monad (join)
import Control.Monad.IO.Class
import Control.Monad.Trans.Reader
import Control.Monad.Trans.Either
import Control.Exception

import System.Util

import Tmux.Util
import Tmux.Types
import Tmux.Classes
import Tmux.Layout
import Tmux.Project.Types
import Tmux.Project.Util

import GHC.Generics
import Control.Newtype

-- | Generic parser for text data encapsulation
parseAlpha :: (Text -> a) -> Value -> Parser a
parseAlpha con val =   con <$> parseJSON val
                  <|> (con . T.pack . show) <$> (parseJSON val :: Parser Int)

parsePath :: (String -> a) -> Value -> Parser a
parsePath con val =  con <$> parseJSON val
                 <|> con . show <$> (parseJSON val :: Parser Int)

instance FromJSON ProjectName  where parseJSON = parseAlpha ProjectName
instance FromJSON Path         where parseJSON = parsePath  Path
instance FromJSON SocketN      where parseJSON = parsePath  SocketN
instance FromJSON SocketP      where parseJSON = parsePath  SocketP
instance FromJSON Pre          where parseJSON = parsePath  Pre
instance FromJSON PreW         where parseJSON = parsePath  PreW
instance FromJSON TOptions     where parseJSON = parsePath  TOptions
instance FromJSON TCommand     where parseJSON = parsePath  TCommand
instance FromJSON Command      where parseJSON = parseAlpha Command

instance ToJSON ProjectName where toJSON (ProjectName val) = toJSON val
instance ToJSON Path        where toJSON (Path        val) = toJSON val
instance ToJSON SocketN     where toJSON (SocketN     val) = toJSON val
instance ToJSON SocketP     where toJSON (SocketP     val) = toJSON val
instance ToJSON Pre         where toJSON (Pre         val) = toJSON val
instance ToJSON PreW        where toJSON (PreW        val) = toJSON val
instance ToJSON TOptions    where toJSON (TOptions    val) = toJSON val
instance ToJSON TCommand    where toJSON (TCommand    val) = toJSON val
instance ToJSON Command     where toJSON (Command     val) = toJSON val
instance ToJSON Layout      where toJSON = String . T.pack . show

instance FromJSON ProjPane where
  parseJSON val =  (PCommand  <$> parseJSON val)
               <|> (PCommands <$> parseJSON val)

instance ToJSON ProjPane where toJSON (PCommand  val) = toJSON val
                               toJSON (PCommands val) = toJSON val


instance FromJSON Layout where
  parseJSON (String val) = return
                         $ either (const defaultLayout) id
                         $ Atto.parseOnly parseL val
  -- TODO: this is just to silence GHC-MOD
  {-parseJSON v = typeMismatch "Expecting a layout string" v-}
  parseJSON _ = fail "Expecting a layout string"


instance FromJSON ProjWindow where parseJSON val =  (SW1 <$> parseJSON val)
                                                   <|> (SW  <$> parseJSON val)
                                                   <|> (MW  <$> parseJSON val)

instance ToJSON ProjWindow where toJSON (SW1 val) = toJSON val
                                 toJSON (SW  val) = toJSON val
                                 toJSON (MW  val) = toJSON val


{-# ANN TmuxinatorYaml ("HLint: ignore Use camelCase"::String) #-}
data TmuxinatorYaml = TmuxinatorYaml
  { name         :: ProjectName
  , root         :: Maybe Path
  , socket_name  :: Maybe SocketN
  , socket_path  :: Maybe SocketP
  , pre          :: Maybe Pre
  , pre_window   :: Maybe PreW
  , tmux_options :: Maybe TOptions
  , tmux_command :: Maybe TCommand
  , windows      :: ProjWindows
  } deriving (Eq,Show,Generic)

jsonOpts :: Options
jsonOpts = defaultOptions { omitNothingFields = True }

toJSON' :: TmuxinatorYaml -> Value
toJSON' = genericToJSON jsonOpts


instance ToJSON   ProjPanes
instance FromJSON ProjPanes
instance ToJSON   TmuxinatorYaml where toJSON = toJSON'
instance FromJSON TmuxinatorYaml

socketToTmuxOptions :: TmuxinatorYaml -> Maybe TmuxCmdLine
socketToTmuxOptions yaml = Just $
  def { socketName = First . fmap unpack . socket_name $ yaml
      , socketPath = First . fmap unpack . socket_path $ yaml
      }

dropSocketsFromTmuxOpts :: Maybe TmuxCmdLine -> Maybe TmuxCmdLine
dropSocketsFromTmuxOpts = fmap (\o -> o { socketName = First Nothing
                                        , socketPath = First Nothing })

tmuxinatorToProject :: TmuxinatorYaml -> EitherT IOException IO Project
tmuxinatorToProject yaml = do
  rt <- normFilePath . maybe "." _path $ root yaml
  tmuxOpts <- case tmux_options yaml of
    Nothing   -> return mempty
    Just opts -> fmap Just . validateTmuxCommandLine . _tOptions  $ opts
  return $ Project
    (name yaml)
    (Path rt)
    (isJust . root $ yaml)
    (pre          yaml)
    (pre_window   yaml)
    (socketToTmuxOptions yaml <> tmuxOpts)
    (tmux_command yaml)
    (windows      yaml)

projectToTmuxinator :: Project -> TmuxinatorYaml
projectToTmuxinator tmux = let
  tmuxOpts'= TOptions . T.unpack <$>. tmuxOptsCmdLine
  in TmuxinatorYaml
    (_projName tmux)
    (boolToMaybe (_projRoot tmux) (_projFixedRoot tmux))
    (fmap SocketN . join . fmap (unpack . socketName) . _projTmuxOptions $ tmux)
    (fmap SocketP . join . fmap (unpack . socketPath) . _projTmuxOptions $ tmux)
    (_projPre         tmux)
    (_projPreWindow   tmux)
    -- TODO: remove socket options, already present in dedicated fields
    (tmuxOpts' . dropSocketsFromTmuxOpts . _projTmuxOptions $ tmux)
    (_projTmuxCommand tmux)
    (_projWindows     tmux)

orderYaml :: Text -> Text
orderYaml yaml' = let
  yaml = T.lines yaml'
  is txt = T.isPrefixOf txt . T.dropWhile (==' ')
  functions = [ is "name"      , is "root"
              , is "socket"    , is "pre"
              , is "pre_window", is "tmux_options"
              , is "tmux_command" ]
  in T.unlines
   $  mapMaybe (`L.find` yaml) functions
   ++ L.filter (\line -> not (L.any (\f -> f line) functions)) yaml

writeYaml :: FilePath -> TmuxinatorYaml -> IO ()
writeYaml = encodeFile

writeHumanReadableYaml :: FilePath -> TmuxinatorYaml -> IO ()
writeHumanReadableYaml fp tmuxYaml
  = T.writeFile fp $ orderYaml . decodeUtf8 . encode $ tmuxYaml

parseTmuxinatorYaml :: BS.ByteString -> EitherT Text IO Project
parseTmuxinatorYaml fc =
  case (decodeEither' fc :: Either ParseException TmuxinatorYaml) of
    Left err -> case parseYaml fc of
      Left err' -> left . tshow $ err'
      Right   _ -> left . tshow $ err
    Right yaml -> toEitherText $ tmuxinatorToProject yaml
              <>>= validateRoot
  where
    toEitherText = bimapEitherT tshow id

saveProjectToTmuxinator
  :: Project    -- ^ Project
  -> FilePath   -- ^ Project full path (including filename)
  -> ReaderT Env (EitherT Text IO) ()
saveProjectToTmuxinator proj pPath = do
  let yaml   = projectToTmuxinator proj
  liftIO $ writeHumanReadableYaml pPath yaml


