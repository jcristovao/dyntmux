{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE DeriveGeneric     #-}
{-# LANGUAGE ConstraintKinds   #-}
{-# OPTIONS_GHC -fno-warn-orphans #-}

module Tmux.Project.Types where

import Data.Text as T
import Data.Map (Map)

import Tmux.Types
import Tmux.Layout
import Tmux.Util

import GHC.Generics
import Data.Default.Generics
import Control.Newtype


-- | Path encapsulation
newtype Path     = Path     { _path     :: FilePath } deriving (Eq,Ord,Show,Generic)
newtype SocketN  = SocketN  { _socketN  :: FilePath } deriving (Eq,Ord,Show,Generic)
newtype SocketP  = SocketP  { _socketP  :: FilePath } deriving (Eq,Ord,Show,Generic)
newtype Pre      = Pre      { _pre      :: FilePath } deriving (Eq,Ord,Show,Generic)
newtype PreW     = PreW     { _preW     :: FilePath } deriving (Eq,Ord,Show,Generic)
newtype TOptions = TOptions { _tOptions :: FilePath } deriving (Eq,Ord,Show,Generic)
newtype TCommand = TCommand { _tCommand :: FilePath } deriving (Eq,Ord,Show,Generic)
newtype ProjectName = ProjectName { _pName :: Text  } deriving (Eq,Ord,Show,Generic)

instance Default Path
instance Default SocketN
instance Default SocketP
instance Default Pre
instance Default PreW
instance Default TOptions
instance Default TCommand
instance Default ProjectName
instance Newtype Path
instance Newtype SocketN
instance Newtype SocketP
instance Newtype Pre
instance Newtype PreW
instance Newtype TOptions
instance Newtype TCommand
instance Newtype ProjectName


data ProjPane = PCommand  { _pCommand  :: Command }
              | PCommands { _pCommands :: Map Text [Command] }
  deriving (Eq,Show)

-- | Pane data
data ProjPanes = ProjPanes
  { layout :: Layout
  , panes  :: [ProjPane]
  } deriving (Eq,Show,Generic)

type ProjWindows= [ProjWindow]
data ProjWindow = SW1 (Map Text Command)
                | SW  (Map Text [Command] )
                | MW  (Map Text ProjPanes )
  deriving (Eq,Show)


commandsFromPane :: ProjPane -> [Command]
commandsFromPane pn = case pn of
  PCommand c -> [c]
  PCommands mp -> snd . singleMapElement $ mp

-- | Main project format
-- All other formats (Yaml, DynTmux, etc) must convert to/from this format
data Project = Project
  { _projName        :: ProjectName
  , _projRoot        :: Path
  , _projFixedRoot   :: Bool
  , _projPre         :: Maybe Pre
  , _projPreWindow   :: Maybe PreW
  , _projTmuxOptions :: Maybe TmuxCmdLine
  , _projTmuxCommand :: Maybe TCommand
  , _projWindows     :: ProjWindows
  } deriving (Eq,Show,Generic)

instance Default Project

