{-# LANGUAGE OverloadedStrings #-}
{-# OPTIONS_GHC -fno-warn-orphans #-}

module Tmux.Project.Util where

import Data.Text as T

import Control.Applicative
{-import Control.Monad (void)-}
import Control.Monad.Trans.Either
import Control.Exception
import System.Util
import System.IO.Error
import System.IO
import GHC.IO.Exception

import Data.Default.Generics
import Tmux.Classes

import Tmux.Project.Types
import Tmux.CmdLine (parseTmuxCmdLine,parseTmuxCmdLine',TmuxCmdLine(..))

leftIOErr :: IOErrorType -> String -> Maybe Handle -> Maybe FilePath
          -> EitherT IOError IO a
leftIOErr = ((.).(.).(.).(.)) left mkIOError

validateRoot :: Project -> EitherT IOException IO Text
validateRoot tmux = T.pack <$> getCustomFolder (Just . _path . _projRoot $ tmux)

{-validateProjTmuxCommandLine :: Project -> EitherT IOException IO ()-}
{-validateProjTmuxCommandLine = validate . _projTmuxOptions-}
  {-where-}
    {-validate Nothing = right ()-}
    {-validate (Just (TOptions t)) = void . validateTmuxCommandLine $ t-}

validateTmuxCommandLine :: String -> EitherT IOException IO TmuxCmdLine
validateTmuxCommandLine tcmdl = case parseTmuxCmdLine' tcmdl of
  Left  _  -> leftIOErr InvalidArgument "Invalid tmux options" Nothing Nothing
  Right cl -> right cl


tmuxOptsCmdLine :: Maybe TmuxCmdLine -> Maybe Text
tmuxOptsCmdLine = fmap (fst . runRdr def . toTmux)
