{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE DeriveDataTypeable #-}
{-# LANGUAGE DeriveGeneric      #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE StandaloneDeriving #-}
{-# LANGUAGE OverlappingInstances #-}
{-# OPTIONS_GHC -fno-warn-orphans #-}

module Tmux.Pane where

import Data.Int
import Data.Maybe
import Data.Data
import Text.Printf
import Data.Text as T hiding (takeWhile)
import Data.Text.Util
import Data.Text.Format
import System.FilePath
import System.Posix.Types

import Control.Applicative
import Control.Monad.Trans.Reader

import GHC.Generics
import Data.Default.Generics
import Control.Newtype
import Language.Haskell.TH

import Tmux.Types
import Tmux.Classes
import Tmux.Util

default (T.Text)

data PaneTitle  = PTitle Text     deriving (Eq,Ord,Typeable,Generic,Show)
data PaneIndex  = PIX Int64       deriving (Eq,Ord,Typeable,Generic,Show)
data PaneId     = PID Int64       deriving (Eq,Ord,Typeable,Generic)
data PanePath   = PPath Text      deriving (Eq,Ord,Typeable,Generic,Show)
data PaneSize   = PSize (Int,Int) deriving (Eq,Ord,Typeable,Generic,Show)
data PaneActive = PActive Bool    deriving (Eq,Ord,Typeable,Generic,Show)
type PaneCurrentCommands = Commands
type PaneStartCommands   = Commands

instance Show PaneId where
  show (PID sid) = '%':show sid

-- TODO: BUG : Last field must not be null
data Pane = Pane
  { paneTitle   :: PaneTitle
  , paneIndex   :: PaneIndex
  , paneId      :: PaneId
  , paneWidth   :: Int64
  , paneHeight  :: Int64
  , panePid     :: ProcessID
  , paneCurrentPath     :: PanePath
  , paneCurrentCommands :: PaneCurrentCommands
  , paneStartCommands   :: PaneStartCommands
  , paneActive  :: PaneActive
  }
  deriving (Eq,Typeable,Generic)

deriving instance Data ProcessID

instance Show Pane where
  show p = printf "%s%s: [%s] (%ix%i) #%s | Path:%s\tCComm:%s\tStartComm:%s"
           (show . paneIndex $ p)
           (show . paneId    $ p)
           (show . paneTitle $ p)
           (paneWidth p)
           (paneHeight p)
           (show . paneActive $ p)
           (show . paneCurrentPath $ p)
           (show . paneCurrentCommands $ p)
           (show . paneStartCommands   $ p)

-- GHC 7.8 / TH 2.9
$(return [])

{-# ANN list_panes ("HLint: ignore Use camelCase"::String) #-}
list_panes :: (Show a, Show b) => a -> b -> Text
list_panes s w = T.pack $  "list-panes -t " ++ show s ++ ":" ++ show w
                        ++ $(getDataMemberTypes =<< reify =<< fmap fromJust (lookupTypeName "Pane"))

-- | Obvious and not so obvious defaults
instance Default PaneTitle
instance Default PaneIndex  where def = PIX 1
instance Default PaneId
instance Default PanePath
instance Default PaneActive
instance Default CPid       where def = -1
instance Default PaneSize   where def = PSize (80,25)
instance Default Pane

instance Newtype PaneTitle
instance Newtype PaneIndex
instance Newtype PanePath
instance Newtype PaneSize
instance Newtype PaneActive

instance ParseTmux PaneTitle  where parse' = parseText PTitle
instance ParseTmux PaneIndex  where parse' = parseNum PIX
instance ParseTmux PaneId     where parse' = parseNumPrefix PID '%'
instance ParseTmux PaneActive where parse' = parseBool PActive
instance ParseTmux PanePath   where parse' = parseText PPath
instance ParseTmux Commands   where parse' = parseTextSingleton Command
instance ParseTmux ProcessID  where parse' = parseNum CPid


instance ToTmux Pane where
  toTmux pane = do
    defShell <- takeFileName . shellEnvName <$> asks sys
    let filterDefShell
          = Prelude.filter (\c -> defShell /= (T.unpack . getCommand $ c))
        indx  = (\(PIX i) -> i) . paneIndex $ pane
    path  <- filterPanePath . (\(PPath p) -> p) . paneCurrentPath $ pane
    let select= l2s $ format "select-pane -t {}\n" $ Only indx
        chdir = if T.null path
                  then T.empty
                  else l2s $ format "send-keys \"{}\" C-m \n"
                           $ Only . escapeCommands $ path
        commands'= T.concat
                 $ Prelude.map (\(Command c) -> l2s
                                              $ format "send-keys \"{}\" C-m \n"
                                              $ Only . escapeCommands $ c)
                 $ filterDefShell
                 $ paneStartCommands pane
        comms = select <> chdir <> commands'
    return (comms,T.empty)

escapeCommands :: Text -> Text
escapeCommands = id
{-escapeCommands = T.intercalate "\\ " . T.words-}
