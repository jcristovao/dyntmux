{-# LANGUAGE OverloadedStrings #-}
module Tmux.Interface where

import System.Process hiding (env)
import System.Exit
import System.Lifted
import System.Directory.Lifted
import System.Util

import Control.Monad
import Control.Monad.Trans.Class
import Control.Monad.Trans.Either
import Control.Monad.Trans.Reader
import Control.Monad.IO.Class
import Control.Exception

import Data.AltComposition
import Data.Easy
import Data.IsNull
import Data.List as L
import Data.Text (Text)
import Data.Text.Util
import qualified Data.Text as T
import qualified Data.Text.IO as T

import Control.Monad.Trans.Either.Exception
import System.Exit.Util
import Tmux.Classes
import Tmux.Types
import Tmux.Util
import Tmux.Session
import Tmux.Window
import Tmux.Pane
import Tmux.CmdLine
import qualified Tmux.Internal.ProcessSession as PS
import qualified Tmux.Internal.ProcessWindow as PW
import qualified Tmux.Internal.ProcessPane as PP

import Data.IORef
import qualified Data.Map    as Map
import qualified Data.IntMap as IM

-- | Split a command into an argument list
argParser :: Text -> [String]
argParser = L.map T.unpack . nonEmpty . parseCommand . T.strip

-- | Execute a command with the given argument list, and return the Output
-- (@STDOUT@) as a Right value if everything was ok, or a Left value with either:
-- * @STDERR@ output
-- * A raised exception text
-- * A non-zero return code associated failure
sh :: String -> Text -> EitherT Text IO Text
sh cmd args = do
  liftIO resetErrno
  ret <- eIOExTxIO $ readProcessWithExitCode cmd (argParser args) ""
  err <- liftIO getLastError
  processResult ret err

processResult
  :: Monad m
  => (ExitCode, String, String)
  -> String
  -> EitherT Text m Text
processResult (exitCode,stdo,stde) err
      | exitCode /= ExitSuccess = left  . T.pack
                                $ stde <!> analyseExitCode exitCode err
      | (not . L.null) stde     = left  . T.pack $ stde
      | otherwise               = right . T.pack $ stdo

-- problem with send keys and commands with spaces...
{-tmux :: Text -> EitherT Text IO Text-}
{-tmux = sh "tmux"-}

tmux' :: Text -> ReaderT FilePath (EitherT Text IO) Text
tmux' comm = ask >>= (\tmx -> lift (sh tmx comm))

runTmux :: Env -> ReaderT FilePath m a -> m a
runTmux = runRdrT . tmuxPath

parseSh :: Text -> [Text]
parseSh = L.filter (not . T.null) . T.split (\c -> c == '\n' || c == '\r')

-- | Terminal like shell do
tshdo :: Text -> ReaderT FilePath IO ExitCode
tshdo what = ask >>= (\tmx -> lift $ rawSystem tmx (argParser what))

getTmuxVersion :: ReaderT FilePath (EitherT Text IO) Text
getTmuxVersion = tmux' "-V"

getSessions :: ReaderT FilePath (EitherT Text IO) Sessions
getSessions = do
  sessionMap <- liftIO $ newIORef Map.empty
  sessions <- tmux' list_sessions
  -- TODO: do we really want to ignore the result??
  forM_ (parseSh sessions) $ \s -> do
    ses <- lift . hoistEither $ PS.process s
    wins <- tmux' $ list_windows (sessionId ses)
    forM (parseSh wins) $ \w -> do
      win <- lift . hoistEither $ PW.process w
      panes <- tmux' $ list_panes (sessionId ses) (windowId win)
      forM (parseSh panes) $ \p -> do
        pane <- lift . hoistEither $ PP.process p
        liftIO $ modifyIORef sessionMap (insertPane ses win pane)
  liftIO . readIORef $ sessionMap

insertPane :: Session -> Window -> Pane -> Sessions -> Sessions
insertPane s w p smap = let
  insertP (nw,nps) (_,ops) = (nw, ops ++ nps)
  insertW = IM.unionWith insertP
  in Map.insertWith insertW s (IM.fromList [(windowIndex w,(w,[p]))]) smap

getCurrentSessionName :: ReaderT FilePath (EitherT Text IO) Session
getCurrentSessionName = do
  session' <- tmux' list_session
           >>= lift . hoistEither . listToEither "No session data found!?" . parseSh
  session  <- lift . hoistEither $ PS.process session'
  return $ Session (SName "") (sessionId session) 0 0 0 (SAttch True)

showSessions :: Sessions -> String
showSessions sessions = let
  showPane = L.concatMap (\p -> "\t\t" ++ show p ++ "\n")
  showWindow  str (w,ps) = str ++ "\t" ++ show w ++ "\n" ++ showPane ps
  showSession str s ws = str ++ show s ++ "\n" ++ IM.foldl' showWindow "" ws
  in Map.foldlWithKey' showSession "" sessions

{-# ANN sessionsToTmux ("HLint: ignore Evaluate"::String) #-}
sessionsToTmux :: Sessions -> Reader Env ([Text],Text)
sessionsToTmux sessions = do
  env   <- ask
  let toTmuxS   =  runRdr env . toTmux
      toTmuxL s = runRdr (env { sessionName' = Just (sessionN s) }) . toTmux
      foldSes lst s wpanes =   fst (toTmuxS s)
                           <> snd . toTmuxS   § s
                           <> fst . toTmuxL s § wpanes
                           <> snd . toTmuxL s § wpanes
                           <> lst
      tmuxLines = nonEmpty . T.lines $ Map.foldlWithKey' foldSes T.empty sessions
      sesName = sessionN. L.head . Map.keys $ sessions
  return (tmuxLines,T.append "attach-session -t " sesName)

checkVersion :: (Int, Int) -> Either Text Text
checkVersion (ma,mi) =
  if ma < 1 || (ma == 1 && mi < 9)
    then Left   errorMsg
    else Right " [Version >= 1.9]"
  where errorMsg = " [Minimum version should be 1.9]\n\n"
         .++ "Fetch it with:\n\n\t"
         .++ "git clone git://git.code.sf.net/p/tmux/tmux-code tmux\n\n"
         .++ "Then run:\n\n\t"
         .++ "cd tmux && sh autogen.sh && ./configure && make && make install\n\n"

gatherReport :: Colour -> [(FilePath,Either Text FilePath)] -> Text
gatherReport c lst = let
  leftJustSz = L.maximum . L.map (L.length . fst) $ lst
  ok = _colour c ?. okT .$ T.pack
  ko = _colour c ?. koT .$ id
  no = _colour c ?. noT .$ T.justifyLeft leftJustSz ' ' . T.pack
  in  T.concat
    . L.map (\(fp,ei) -> no fp <> " : " <> either ko ok ei <> "\n")
    $ lst

toTextEitherT :: (Functor m, Monad m) => EitherT IOException m a -> EitherT Text m a
toTextEitherT = bimapEitherT (T.pack . show) id

{-# ANN analyseEnv ("HLint: ignore Use head"::String) #-}
analyseEnv
  :: Report
  -> Colour
  -> Lenient
  -> FilePath
  -> EitherT Text IO ShellEnv
analyseEnv reprt color lenientTCheck tmuxName = do
  let findTmux tn = joinMET (tn .<> " not found")
                  . toTextEitherT
                  . (findExecutable :: FilePath -> EitherT IOException IO (Maybe FilePath))
                  $ tn
      tmuxVersionEval tn = do
        tmuxLoc <- findTmux tn
        fmap T.unpack . joinEET . fmap checkVersion
                      . joinMET "cannot parse tmux version"
                      . fmap parseTmuxVersion $ runRdrT tmuxLoc getTmuxVersion

      actions =
        [ ("SHELL" , toTextEitherT . getShellExe)
        , ("EDITOR", toTextEitherT . getShellExe)
        , ("CWD"   , toTextEitherT . const getCurrentDirectory)
        , (tmuxName, findTmux )
        , (tmuxName, tmuxVersionEval)
        ]
  -- extract results
  results <- liftIO $ mapM (\(v,f) -> runEitherT . f $ v) actions
  -- gather report and status
  let rep = gatherReport color $ L.zip (L.map fst actions) results
      res = catEithers results
  -- if report flag active, print to screen
  when (_report reprt) (liftIO . T.putStrLn $ rep)
  -- and finally, report back
  if L.all isRight results
    then right $ ShellEnv (res !! 0) (res !! 1) (res !! 2) (res !! 3)
    else if _lenient lenientTCheck && L.all isRight (initDef [Left ""] results)
           then right $ ShellEnv (res !! 0) (res !! 1) (res !! 2)
                                 (either (const "") id $ results !! 3)
           else left rep


