{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RebindableSyntax  #-}
{-# LANGUAGE NoMonomorphismRestriction #-}

module Tmux.Project where

import Prelude
import Data.String
import Safe
import Control.Applicative
import Control.Monad.Trans.Class
import Control.Monad.Trans.Either
import Control.Monad.Trans.Reader
import Control.Monad.IO.Class
import Control.Monad

import Data.Text (Text)
import qualified Data.Text as T
import qualified Data.IntMap as IM
import Data.Maybe
import Data.Int
import qualified Data.List as L
import qualified Data.Map  as Map
import qualified Data.Char as Ch

import Data.Easy
import Data.AltComposition
import Data.Monoid
import Data.Default.Generics
import System.FilePath
import System.Lifted
import System.Directory.Lifted
import Control.Newtype

import qualified Data.ByteString as BS

import Tmux.Util
import Tmux.Layout
import Tmux.Types
import Tmux.Classes
import Tmux.Session
import Tmux.Pane
import Tmux.Interface
import Tmux.ProcessTree
import Tmux.Window  (windowN )
import qualified Tmux.Window as W
import qualified Tmux.Pane   as P

import Tmux.Project.Types
import Tmux.Project.Rules
import qualified Tmux.Project.Tmuxinator as PN

import Tmux.Folders

{-import Debug.Trace-}

loadProject :: FilePath -> EitherT Text IO Project
loadProject fp = do
  -- TODO: fix this
  bs <- liftIO . BS.readFile $ fp
  case fmap Ch.toUpper . takeExtension $ fp of
    ".YAML"       -> PN.parseTmuxinatorYaml   bs
    ".YML"        -> PN.parseTmuxinatorYaml   bs
    ".TMUXINATOR" -> PN.parseTmuxinatorYaml   bs
    _             -> PN.parseTmuxinatorYaml   bs

saveProject
  :: Maybe ProjectFormat
  -> FilePath         -- ^ Project Location
  -> Maybe FilePath   -- ^ Optional project filename
  -> Project          -- ^ Project
  -> ReaderT Env (EitherT Text IO) ()
saveProject mFormat projLoc cmdLineName proj = do
  env <- ask
  let pjname = projFilename projLoc cmdLineName (_pName . _projName $ proj)
      format = getFirst $  First mFormat
                        <> First . getProjectFormatStrict § pjname
                        <> First . Just . saveExtension . configur § env
  case format of
    Nothing -> lift (left "Cannot determine destination project format")
    Just f  -> case f of
      YamlTmuxinator -> PN.saveProjectToTmuxinator  proj
                                                   (projAddFileExtension f pjname)
      -- TODO: add save to dyntmux project
      YamlDynTmux    -> undefined


sessionToProject
  :: Path -- projec root
  -> Bool -- fixed root
  -> (Session,W.WindowPanes) -- session name and window panes
  -> TmuxProcs
  -> Reader Env Project
sessionToProject rt fixed_rt (session,winPanes) tprocs = do
  wins <- windowsToProject winPanes tprocs
  return def { _projName      = ProjectName . sessionN $ session
             , _projRoot      = rt
             , _projFixedRoot = fixed_rt
             , _projWindows   = wins
             }

windowsToProject :: W.WindowPanes -> TmuxProcs -> Reader Env ProjWindows
windowsToProject wpanes tprocs = do
  let wpanes' = L.filter ( L.any (`processIdInTmuxProcs` snd tprocs)
                         . fmap panePid . snd)
              . IM.elems $ wpanes
  mapM (windowPanesToWindow tprocs) wpanes'
  {-return . L.map (windowPanesToWindow curPath defPath tprocs) $ wpanes'-}

paneToCommands :: TmuxProcs -> Configuration -> Pane -> [(PsType,Command)]
paneToCommands (_,ptl) config pn = let
  f = case saveBehaviour config of
    WhiteListing -> takeWhile (prefixInWordList (saveList config) . snd)
    BlackListing -> takeWhile (not . prefixInWordList (saveList config) . snd)
  in fmap (Command . T.pack) <$> f . getProcessIdCommands (panePid pn) § ptl

-- | If not found, don't insert
insertAfter :: (a -> Bool) -> a -> [a] -> [a]
insertAfter test el lst = let
  (before,after) = L.break test lst
  in L.null after ?  lst $ before ++ [L.head after] ++ [el] ++ tailSafe after

-- | Tmux puts current process in non-active window/pane as stopped
-- we don't need to force that, hence the different behaviour for the last
-- item
-- Also, hugly hack to exclude dyntmux itself from the command list
renderCommands :: [(PsType,Command)] -> Commands
renderCommands = go . L.takeWhile (not . T.isInfixOf "dyntmux " . unpack .  snd)
  where
    go [] = []
    go xs = let
      init' = fmap (\(t,c) -> t == PsStop ? over Command (<> " &") c $ c) $ init xs
      last' = snd . last $ xs
      in init' ++ [last']


windowPanesUpdateCommands
  :: TmuxProcs
  -> (W.Window, [Pane])
  -> Reader Env (W.Window, [Pane])
windowPanesUpdateCommands tprocs (win,ps) = do
  defPath <- asks defaultPath
  curPath <- shellEnvCwd <$> asks sys
  config  <- asks configur
  let commands' :: Pane -> [(PsType, Command)]
      commands'    = paneToCommands tprocs config   {- P.paneCurrentCommands-}
      relevPaths p = guessFilesFromCommands . fmap (T.unpack . unpack . snd)
                                            . commands'
                                            $ p
      destDir    p = T.unpack . T.strip . T.pack
                   $ getDirToGo p defPath curPath (relevPaths p)
      chdir      p = filePathToCommand $ "cd " ++ destDir p
      isShell'     = (==PsLoginSh) . fst
      commands_  p = shouldChDir p defPath curPath && atLeast 1 (destDir p)
                      ? insertAfter isShell' (PsNormal,chdir p) (commands' p)
                      $ commands' p
  return ( win
     , fmap (\p -> p { paneCurrentCommands = renderCommands (commands_ p)}) ps)

-- this function does a fucking lot: too much.
-- It identifies if there is
-- * a single pane/window/command
-- * or a single pane/window/multiple commands
-- * or multiple panes per window, each with one or multiple commands
-- besides that, it also checks if it is necessary to change directory
-- thus adding a command (to update the relevant directory)
windowPanesToWindow
  :: TmuxProcs
  -> (W.Window, [Pane])
  -> Reader Env ProjWindow
windowPanesToWindow tprocs (win', ps') = do
  defPath <- asks defaultPath
  curPath <- shellEnvCwd <$> asks sys
  (win,ps)<- windowPanesUpdateCommands tprocs (win',ps')
  return $ getProjWindow defPath curPath win ps
  where
    fstNOnlyPane = headNote "Incoherent panes list"
    commands'    = P.paneCurrentCommands

    -- first word of command should be used as pane title
    firstWord = headNote "cannot extract first word of pane command"
              . T.words
              . (\(Command c) -> c)
              . headNote "first command"

    commandToPane co = case co of
      []     -> error "A pane should have at least one command"
      (c:[]) -> PCommand c
      cs     -> PCommands (Map.singleton (firstWord cs) cs)

    onePaneOneCommandSameDir curPath' defPath' pans
      =  L.length pans == 1
      && L.length (commands' . fstNOnlyPane $ pans) <= 1
      && not (shouldChDir (fstNOnlyPane ps') defPath' curPath')


    getProjWindow defPath curPath win ps
      | onePaneOneCommandSameDir curPath defPath ps
        = let comm = headDef (Command "") (commands' . fstNOnlyPane $ ps)
          in  SW1 $ Map.fromList [(windowN win, comm)]
      | L.length ps == 1
        = let comm' = commands' . fstNOnlyPane $ ps
          in SW $ Map.fromList [(windowN win,comm')]
      | otherwise
        = MW $ Map.fromList [( windowN win
                             , ProjPanes
                                 (W.windowLayout win)
                                 -- TODO: fix this
                                 (L.map (commandToPane . commands') ps)
                             )]

-- given the actual screen size and a list of windows
-- prefer the biggest dimensions in layout, but if not found
-- use screen size
findLayoutSize :: (Int64, Int64) -> [ProjWindow] -> (Int64, Int64)
findLayoutSize (x,y) ws
  = fromMaybe (x,y)
  $ join
  $ fmap getLayout
  $ L.find hasLayout ws
  where
    hasLayout w = case w of
      MW _ -> True
      _    -> False
    getLayout (MW mp) = maxDimensions . layout . L.head . Map.elems $ mp
    getLayout _ = Nothing


{-# ANN projectToWindows ("HLint: ignore Evaluate"::String) #-}
projectToWindows :: ProjWindows -> Reader Env W.WindowPanes
projectToWindows ws = do
  (x,y) <- asks screenSize
  defpath <- getDefPath
  let
    defLayout = Layout 0 LayoutEvenHorizontal
                [LayoutUnit x y 0 0 (LCPaneId (P.PID 0))]
    win :: Text -> Int -> Int -> Layout -> W.Window
    win nm i ps lay
      = W.Window (W.WName nm)
        i (W.WID (fromIntegral i))
        (fst $ findLayoutSize (x,y) ws)
        (snd $ findLayoutSize (x,y) ws)
        (fromIntegral ps)
         lay
        [W.WNormal]
        (W.WActive True)
    insertIM :: (Int,W.WindowPanes) -> ProjWindow -> (Int,W.WindowPanes)
    insertIM (i,im) w = case w of
      SW1 mp -> let
        (nm,comm) = singleMapElement mp
        in (i+1,IM.insert i (win nm i 1 defLayout, insertSP [comm]) im)
      SW mp -> let
        (nm,comms) = singleMapElement mp
        in (i+1,IM.insert i (win nm i 1 defLayout, insertSP comms) im)
      MW mp -> let
        (nm,pns) = singleMapElement mp
        lay' = fillLayoutUnits' (layout pns)
                                (fromIntegral . L.length . panes $ pns)
                                (x,y)
        in (i+1,IM.insert i ( win nm i (len . panes $ pns) lay'
                            , insertMP (panes pns) lay')
            im
            )
    insertSP :: Commands -> [P.Pane]
    insertSP comms = [emptyPane (1 :: Int64)
                                (fst $ findLayoutSize (x,y) ws)
                                (snd $ findLayoutSize (x,y) ws)
                                defpath
                                comms -- was comm
                                True ]

    insertMP :: [ProjPane] -> Layout -> [P.Pane]
    insertMP pns lay'
      = snd $ L.foldr (\(pane,(w',h',_,_)) (i,lst) ->
                       (i-1,emptyPane i w' h' defpath (commandsFromPane pane) True :lst)
                      ) (L.length pns,[])
                  $ L.zip pns $ layoutToList lay'
    len = fromIntegral . L.length

  return . snd $ L.foldl' insertIM (1,IM.empty) ws

  where
    emptyPane i x y pth cmds act
      = def { paneIndex        = P.PIX (fromIntegral i)
            , paneWidth        = x
            , paneHeight       = y
            , paneCurrentPath  = P.PPath pth
            , paneStartCommands= cmds
            , paneActive       = P.PActive act }


-- root information is lost here
projectToSessions :: Project -> Reader Env Sessions
projectToSessions tmux = do
  (x,y) <- asks screenSize
  let ws  = _projWindows tmux
      len = fromIntegral . L.length $ ws
      nm  = _pName . _projName $ tmux
      modEnv e = _projFixedRoot tmux
                   ? e { defaultPath = Just . _path . _projRoot $ tmux }
                   $ e
  wps <- local modEnv $ projectToWindows ws
  return $ Map.fromList [ ( Session (SName nm) (SID 1) x y len (SAttch True)
                        , wps)]

saveCurrentSessionToProject
  :: ReaderT Env (EitherT Text IO) Project
saveCurrentSessionToProject = do
  env <- ask
  cf <- shellEnvCwd <$> asks sys
  rt <- fromMaybe cf <$> asks defaultPath
  sessions <- lift $ runTmux env getSessions
  currSess <- lift $ runTmux env getCurrentSessionName
  sesPanes <- lift $ hoistEither $ case Map.lookupLE currSess sessions of
    Nothing -> Left "Current session is new! Please repeat the command."
    Just (s,pns) -> Right (s,pns)
  procTree <- lift getTmuxProcs
  return $ runRdr (env { sessionName' = Just . sessionN $ currSess })
         $ sessionToProject (Path rt) True sesPanes procTree

listProjects :: FilePath -> ReaderT Env IO ()
listProjects locatPath = do
  colour <- asks colourOutput
  liftIO $ do
    files <- L.filter isYaml <$> evalEitherT . getDirectoryContents § locatPath
    res <- mapM (\fn -> runEitherT $ loadProject (locatPath </> fn)) files
    mapM_ (Prelude.putStrLn . evalProjectStatus (_colour colour))
        $ L.zip files res

wipeProjects :: ProjLocation -> IO ()
wipeProjects location = do
  (_,locatPath) <- chooseDefProjFolder location
  files <-  L.map (locatPath </>) . L.filter isYaml
        <$> evalEitherT . getDirectoryContents § locatPath
  mapM_ (evalEitherT . removeFile) files


