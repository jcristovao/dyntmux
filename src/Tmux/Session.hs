{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE DeriveDataTypeable #-}

module Tmux.Session where

import Data.Int
import Data.Data
import Data.Function
import qualified Data.Map as Map

import Data.Text as T
import Data.Text.Format
import Text.Printf
import Data.Text.Util

import Control.Monad.Trans.Reader

import Language.Haskell.TH

import Tmux.Types
import Tmux.Classes
import Tmux.Util
import Tmux.Window

default (T.Text)

newtype SessionName    = SName Text      deriving (Eq,Ord,Typeable,Data,Show)
newtype SessionId      = SID Int64       deriving (Eq,Ord,Typeable,Data)
newtype SessionWindows = SWins Int64     deriving (Eq,Ord,Typeable,Data,Show)
newtype SessionAttached= SAttch Bool     deriving (Eq,Ord,Typeable,Data,Show)

instance Show SessionId where
  show (SID sid) = '$':show sid

data Session = Session
  { sessionName     :: SessionName
  , sessionId       :: SessionId
  , sessionWidth    :: Int64
  , sessionHeight   :: Int64
  , sessionWindows  :: Int64
  , sessionAttached :: SessionAttached
  }
  deriving (Typeable,Data)

instance Eq  Session where (==)    = (==)    `on` sessionId
instance Ord Session where compare = compare `on` sessionId

instance Show Session where
  show s = printf "%s: (%s) (%ix%i) %i %s"
            (show . sessionId   $ s)
            (show . sessionName $ s)
            (sessionWidth   s)
            (sessionHeight  s)
            (sessionWindows s)
            (show . sessionAttached $ s)

instance ToTmux Session where
  toTmux s = do
    (x,y) <- asks screenSize
    let size = l2s $ format " -x {} -y {} " (x,y)
    return ( "new-session -Ad -s "
                 <> sessionN s
                 <> size
                 <> "\n"
                  , T.empty)

sessionN :: Session -> Text
sessionN = (\(SName nm) -> nm) . sessionName

-- GHC 7.8 / TH 2.9
$(return [])

{-# ANN list_session ("HLint: ignore Use camelCase"::String) #-}
list_session :: Text
list_session = T.pack $ "display-message -p"
                      ++ $(getDataMemberTypes =<< reify ''Session)

{-# ANN list_sessions ("HLint: ignore Use camelCase"::String) #-}
list_sessions :: Text
list_sessions = T.pack $  "list-sessions"
                       ++ $(getDataMemberTypes =<< reify ''Session)

instance ParseTmux SessionName      where parse' = parseText SName
instance ParseTmux SessionId        where parse' = parseNumPrefix SID '$'
instance ParseTmux SessionAttached  where parse' = parseBool SAttch

type Sessions = Map.Map Session WindowPanes

