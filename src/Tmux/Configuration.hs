{-# LANGUAGE OverloadedStrings         #-}
{-# LANGUAGE DeriveGeneric             #-}
{-# LANGUAGE NoMonomorphismRestriction #-}

module Tmux.Configuration where

import Prelude
import Control.Applicative

import qualified Data.Text as T
import Data.Maybe

import Data.Monoid

import Data.Yaml

import GHC.Generics

import qualified Data.ByteString as BS
import Data.Attoparsec.Text.Parsec

import Tmux.Types


{-# ANN module ("HLint: ignore Use camelCase"::String) #-}
data ConfigurationJson = ConfigurationJson
  { default_save_extension :: FilePath
  , default_save_behaviour :: FilePath
  , whitelist              :: Maybe [FilePath]
  , blacklist              :: Maybe [FilePath]
  } deriving (Eq,Generic)

instance ToJSON   ConfigurationJson
instance FromJSON ConfigurationJson

-- | Transform parsed JSON data structure into usable configuration
-- Fault forgiving, assumes Blacklisting and Dyntmux format in case
-- of doubt
configurationFromJson :: ConfigurationJson -> Configuration
configurationFromJson cjson = let
  beh = either (const BlackListing) (const WhiteListing)
      . flip parseOnly (T.pack . default_save_behaviour $ cjson)
      $  try( skipSpace >> string "white"       )
     <|> try( skipSpace >> string "whitelist"   )
     <|> try( skipSpace >> string "whilelisting")
  ext = either (const YamlDynTmux) (const YamlTmuxinator)
      . flip parseOnly (T.pack . default_save_extension $ cjson)
      $  try( skipSpace >> string "yml" )
     <|> try( skipSpace >> string "YML" )
     <|> try( skipSpace >> string "yaml")
     <|> try( skipSpace >> string "YAML")
  lst = case beh of
    WhiteListing -> fromMaybe [] (whitelist cjson)
    BlackListing -> fromMaybe [] (blacklist cjson)
  in Configuration ext beh lst

loadConfiguration :: FilePath -> IO Configuration
loadConfiguration fp = do
  bs         <- BS.readFile fp
  return $ case decodeEither' bs of
    Left err -> error ("Invalid dyntmux configuration file:" <> show err)
    Right js -> configurationFromJson js

