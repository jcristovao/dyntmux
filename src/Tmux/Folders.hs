{-# LANGUAGE OverloadedStrings    #-}
{-# LANGUAGE TupleSections        #-}
{-# OPTIONS_GHC -fno-warn-orphans #-}

{-# LANGUAGE CPP #-}

module Tmux.Folders where

import Data.Easy
import Data.AltComposition
import Data.List
import Data.Text (Text)
import qualified Data.Text as T
import Data.Text.Util

import System.Lifted
import System.Directory.Lifted
import System.Util
import System.FilePath
import GHC.IO.Exception
import qualified Data.IsNull as N

import Control.Monad (join)
import Control.Monad.Trans.Either
import Control.Applicative
import SimpleGit

import Paths_dyntmux

data ProjLocation
  = LocatGit    -- ^ Git repository we might be in
  | LocatSmart
  | LocatConfig -- ^ User configuration (typically ~/.config/dyntmux)
  | LocatHome   -- ^ Home folder
  | LocatCurr   -- ^ Current folder
  | LocatNator  -- ^ Tmuxinator configuration
  deriving (Eq,Show)

dyntmuxConfig :: FilePath
dyntmuxConfig = "dyntmux"

dyntmuxConfig' :: FilePath
dyntmuxConfig' = '.' : dyntmuxConfig

emptyConfig :: FilePath
emptyConfig = ""

dyntmuxConfigurationFile :: FilePath
dyntmuxConfigurationFile = "dyntmux.config"


getNatorFolder :: EitherT IOException IO FilePath
getNatorFolder = do
  home   <- isRW =<< getHomeDirectory
  isRW $ home </> ".tmuxinator"

-- | Choose the dyntmux configuration folder
chooseConfigFolder :: IO FilePath
chooseConfigFolder = do
  config <- runEitherT getXdgConfigFolder
  config'<- runEitherT getDotConfigFolder
  evalEither $ config <|> config'

-- | Gets the configuration file path.
-- Will create one, in necessary
-- TODO: does not work
getConfigFileName :: IO FilePath
getConfigFileName = do
  folder <- chooseConfigFolder
  let fp  = folder </> dyntmuxConfigurationFile
  exists <- (runEitherT . doesFileExist $ fp) :: IO (Either IOException Bool)
  case exists of
    Right True -> return fp
    _          -> do
      {-defFn <- getDataFileName dyntmuxConfigurationFile-}
      defFn <- return dyntmuxConfigurationFile
      evalEitherT $ copyFile defFn fp
      return fp

-- | Given a prefered location, return the efective choice (upon availability)
-- and corresponding path for the dyntmux configuration file
chooseDefProjFolder :: ProjLocation -> IO (ProjLocation,FilePath)
chooseDefProjFolder pref = do
  config <- fmap (fmap (LocatConfig,)) $ runEitherT   getXdgConfigFolder
  config'<- fmap (fmap (LocatConfig,)) $ runEitherT   getDotConfigFolder
  home   <- fmap (fmap (LocatHome  ,)) $ runEitherT   getHomeFolder
  git    <- fmap (fmap (LocatGit   ,)) $ runEitherT $ getGitBase "."
  curr   <- fmap (fmap (LocatCurr  ,)) $ runEitherT   getCurrFolder
  nator  <- fmap (fmap (LocatNator ,)) $ runEitherT   getNatorFolder
  evalEither $ case pref of
        LocatGit    -> git
        LocatSmart  -> git <|> config <|> config' <|> home <|> curr <|> nator
        LocatConfig -> config <|> config' <|> home
        LocatHome   -> home
        LocatCurr   -> curr
        LocatNator  -> nator

-- | Determine the base folder for all relative paths present in the configuration
-- file.
chooseBaseFolder :: Maybe FilePath -> IO FilePath
chooseBaseFolder mfpath = do
  git    <- runEitherT $ getGitBase "."
  curr   <- runEitherT   getCurrFolder
  custom <- runEitherT $ getCustomFolder mfpath
  evalEither $ case mfpath of
                Nothing -> git <|> curr
                _       -> custom

-- | Given a prefered location, determine the possible location using
-- @chooseconffolder@, and then create a configuration folder for dyntmux.
-- This corresponds to @dyntmux@ in @/home/user/.config/@, or
-- @.dyntmux@ in either @/home/user/@ or @gitbase@. If the location is
-- @.tmuxinator@ or the current folder, no special sub-folder is created.
initProjLoc :: ProjLocation -> IO FilePath
initProjLoc pref = do
  (loc,base) <- chooseDefProjFolder pref
  let dyntmux = case loc of
        LocatHome -> dyntmuxConfig'
        LocatGit  -> dyntmuxConfig'
        LocatNator-> emptyConfig
        LocatCurr -> emptyConfig
        _         -> dyntmuxConfig
  evalEitherT $ canonicalizePath =<< checkOrCreate (base </> dyntmux)

isYaml :: FilePath -> Bool
isYaml flnm = takeExtension flnm `elem` [".yml",".yaml",".YML",".YAML"]

fmapL :: (Functor m, Monad m) => (a -> b) -> EitherT a m c -> EitherT b m c
fmapL f = bimapEitherT f id

-- | Either locate the single config in the provided location, or examine
-- the provided configuration path.
projFile :: FilePath -> Maybe FilePath -> EitherT Text IO FilePath
projFile projFolder mName
  -- file was specified, no yaml extension, or no file specified
  | (maybe True not . fmap isYaml &&\ isJust $ mName) || isNothing mName = do
        contents <- fmapL (T.pack . show)
                  $ (getDirectoryContents projFolder :: EitherT IOException IO [FilePath])
        case filter isYaml contents of
          []     -> left $ noProjErr projFolder
          (x:[]) -> if getFilename mName `isInfixOf` x && isJust mName
                      then checkFile projFolder x
                      else left $ if isJust mName then wrongProjErr projFolder
                                                  else noProjErr projFolder
          xs -> if isJust mName
                  then case bestMatch (getFilename mName) xs of
                    Nothing -> left $ wrongProjErr projFolder
                    Just x  -> checkFile projFolder x
                else left $ lotsProjErr projFolder
  | otherwise = checkFile projFolder . getFilename $ mName
      where
        noProjErr  wher   = "No project found in "
                          <>. wher <> " or specified on the command line"
        lotsProjErr wher  = "More than one project found in "
                          <>. wher <> ", and no name provided"
        wrongProjErr wher = "Specified project does not exist in " <>. wher
        checkFile dir file = let path = dir </> file
                              in bimapEitherT tshow (const path) (isRW path)
        getFilename = join . maybeToList
        bestMatch term lst = list Nothing
                                  (Just . minimumBy (compare `on` length))
                                  (filter (isPrefixOf term) lst)

{-projFile proj (Just filename) = do-}
  {-let path = trace (show proj ++ " </> " ++ show filename) $ proj </> filename-}
  {-bimapEitherT (T.pack . show) (const path) (isRW path)-}

{-initConf :: IO FilePath-}
{-initConf = do-}
  {-xpto <- runEitherT $ do-}
    {-home   <- getHomeFolder-}
    {-config <- getXdgConfigFolder <§> getDotConfigFolder-}
    {-let vconfig = config </> dyntmuxConfig-}
    {-checkOrCreate vconfig <§> checkOrCreate (home </> dyntmuxConfig')-}
  {-evalEither xpto-}

  {-config <- runEitherT (getXdgConfigFolder <|> getDotConfigFolder)-}
  {-home   <- runEitherT getHomeFolder-}
  {-let vconfig = fmap (</> dyntmuxConfig) config-}
  {-validConfig0 <- runEitherT $ checkOrCreate vconfig-}
  {-case validConfig0 of-}
    {-Right p -> return p-}
    {-Left  _ -> evalEitherT $ checkOrCreate home dyntmuxConfig'-}


