{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE ExtendedDefaultRules #-}
{-# LANGUAGE DeriveDataTypeable #-}
{-# LANGUAGE TemplateHaskell #-}
{-# OPTIONS_GHC -fno-warn-type-defaults #-}
{-# OPTIONS_GHC -fno-warn-unused-do-bind #-}
module Tmux.Internal.ProcessSession where

import Data.List as L
import Data.Text as T
import Data.Either.Combinators
import Data.Attoparsec.Text.Parsec
import Control.Applicative

import Language.Haskell.TH
import Tmux.Classes
import Tmux.Util
import Tmux.Session

process :: Text -> Either Text Session
process s
  = mapLeft T.pack 
  $ flip parseOnly s
    $(L.foldr
        (\x a -> uInfixE a [|(<*>)|] (varE x))
        (appE [|return|] (conE 'Session))
        (L.replicate $(litE . integerL . getCount =<< reify ''Session) 'parse'))



