{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE ExtendedDefaultRules #-}
{-# LANGUAGE DeriveDataTypeable #-}
{-# LANGUAGE TemplateHaskell #-}
{-# OPTIONS_GHC -fno-warn-type-defaults #-}
{-# OPTIONS_GHC -fno-warn-unused-do-bind #-}
module Tmux.Internal.ProcessPane where

import Data.List as L
import Data.Text as T
import Data.Either.Combinators
import Data.Attoparsec.Text.Parsec
import Control.Applicative

import Language.Haskell.TH
import Tmux.Classes
import Tmux.Util
import Tmux.Pane

process :: Text -> Either Text Pane
process s
  = mapLeft T.pack 
  $ flip parseOnly s
    $(L.foldr
        (\x a -> uInfixE a [|(<*>)|] (varE x))
        (appE [|return|] (conE 'Pane))
        (L.replicate $(litE . integerL . getCount =<< reify ''Pane) 'parse'))



