{-# LANGUAGE OverloadedStrings #-}
{-# OPTIONS_GHC -fno-warn-orphans #-}
module Tmux.Classes where

import Data.Monoid
import Data.Text (Text)
import qualified Data.Text as T
import Data.Attoparsec.Text.Parsec
import Control.Monad.Trans.Reader
import Generics.Deriving.Monoid
import Control.Newtype
import Tmux.Types

type ToTmuxBeforeAfter = (Text,Text)

-- fst: to be executed for object creation
-- snd: to be executed after dependent object(s) creation
class ToTmux a where
  toTmux :: a -> Reader Env ToTmuxBeforeAfter

class ParseTmux a where
  parse' :: Parser a

class ParseLayout a where
  parseL :: Parser a

runRdr :: r -> Reader r a -> a
runRdr env f = runReader f env

runRdrT :: r -> ReaderT r m a -> m a
runRdrT env f = runReaderT f env

filterPanePath :: Text -> Reader Env Text
filterPanePath path = do
  defPath <- asks defaultPath
  curPath <- fmap shellEnvCwd . asks $ sys
  return $ case defPath of
    Just defp -> if T.unpack path == defp    then "" else path
    Nothing   -> if T.unpack path == curPath then "" else path

getDefPath :: Reader Env Text
getDefPath = do
  defPath <- asks defaultPath
  curPath <- fmap shellEnvCwd . asks $ sys
  return $ case defPath of
    Just defp -> T.pack defp
    Nothing   -> T.pack curPath

instance Monoid TmuxCmdLine where
  mempty  = memptydefault
  mappend = mappenddefault

instance ToTmux TmuxCmdLine where
  toTmux tcmdl = return $ (\x -> (x,"") :: (Text,Text))
               $  ("-2"  ?? c256         tcmdl)
               >< ("-C"  ?? controlMode  tcmdl)
               >< ("-l"  ?? loginShell   tcmdl)
               >< ("-q"  ?? quiet        tcmdl)
               >< ("-u"  ?? utf8         tcmdl)
               >< ("-v"  ?? verbose      tcmdl)
               >< ("-c " ?@ shellCommand tcmdl)
               >< ("-f " ?@ altConfig    tcmdl)
               >< ("-L " ?@ socketName   tcmdl)
               >< ("-S " ?@ socketPath   tcmdl)
               >< (mcomms . commands $ tcmdl)
    where (??) val bnt = if unpack bnt then val else mempty
          (?@) val = maybe mempty ((val <>) . T.pack) . unpack
          (><) a b | a == mempty && b == mempty = mempty
                   | a == mempty                = b
                   | b == mempty                = a
                   | otherwise                  = a <> " " <> b
          mcomms   = maybe mempty ((" " <>) . T.intercalate " " . map T.pack)


