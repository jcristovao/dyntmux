{-# LANGUAGE OverloadedStrings   #-}
{-# LANGUAGE NoMonomorphismRestriction #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE DeriveGeneric       #-}

module Tmux.ProcessTree
  ( PsProcessState(..)
  , PsProcessPrio (..)
  , PsProcessStateExtra(..)
  , ProcessState(..)
  , PsProcess(..)
  , PsType(..)
  , ProcessTree
  -- returned data type
  , TmuxProcs
  -- main function
  , getTmuxProcs
  , processIdInTmuxProcs
  , getProcessIdCommands

  , findTmuxServers
  , identifyMyTmux
  , getChildProcesses
  -- Debug
  , printTmuxTree
  , printProcessTree

  ) where

import Prelude hiding (takeWhile)
import Data.Text (Text)
import qualified Data.Text as T
import qualified Data.Foldable as F

import Control.Monad.IO.Class
import Control.Monad.Trans.Either

import Tmux.Interface
import System.Posix.Types
import Data.Attoparsec.Text.Parsec
import Data.Char
import GHC.Generics
import Data.AltComposition
import Data.Easy
import Data.Monoid
import qualified Data.List as L
import Data.Tree as Tree
import qualified Data.Tree.Util as Tree
import System.Posix.Process
import Control.Applicative
import qualified Data.Sequence as Seq

import Generics.Deriving.Monoid
import Control.Newtype

import qualified Tmux.Util as Util
import Tmux.Util ((<$>.))

-- D    uninterruptible sleep (usually IO)
-- R    running or runnable (on run queue)
-- S    interruptible sleep (waiting for an event to complete)
-- T    stopped, either by a job control signal or because it is being traced
-- W    paging (not valid since the 2.6.xx kernel)
-- X    dead (should never be seen)
-- Z    defunct ("zombie") process, terminated but not reaped by its parent
data PsProcessState = PsD | PsR | PsS | PsT | PsW | PsX | PsZ
  deriving (Eq,Ord,Show,Generic)

-- Process Priority: Low, Normal, High
data PsProcessPrio = PpL | PpN | PpH
  deriving (Eq,Ord,Show,Generic)

-- <    high-priority (not nice to other users)
-- N    low-priority (nice to other users)
-- L    has pages locked into memory (for real-time and custom IO)
-- s    is a session leader
-- l    is multi-threaded (using CLONE_THREAD, like NPTL pthreads do)
-- +    is in the foreground process group
data PsProcessStateExtra = StateExtra
  { sePrio         :: PsProcessPrio
  , seLockedPages  :: Bool
  , seIsSessLeader :: Bool
  , seIsMultiThread:: Bool
  , seIsForeground :: Bool
  } deriving (Eq,Ord,Show,Generic)

notDead :: PsProcess -> Bool
notDead = (`notElem` [PsD,PsW,PsX,PsZ]) . baseState . state

data ProcessState = ProcessState
  { baseState :: PsProcessState
  , extraState:: PsProcessStateExtra
  } deriving (Eq,Ord,Show,Generic)

psStateParser :: Parser ProcessState
psStateParser = do
    base <-  'D' ~> PsD <|> 'R' ~> PsR <|> 'S' ~> PsS
         <|> 'T' ~> PsT <|> 'W' ~> PsW <|> 'X' ~> PsX <|> 'Z' ~> PsZ
    prio <-  '<' ~> PpH <|> 'N' ~> PpL <|> return PpN
    lock <-  chk 'L' <|> return False
    lead <-  chk 's' <|> return False
    mult <-  chk 'l' <|> return False
    fore <-  chk '+' <|> return False
    return $ ProcessState base (StateExtra prio lock lead mult fore)
  where
    (~>) ch pst = try (const pst  <$> char ch)
    chk  ch     = try (const True <$> char ch)


data PsProcess = PsProcess
  { selfPID     :: ProcessID
  , parentPID   :: ProcessID
  , state       :: ProcessState
  , selfTty     :: Text
  , loginSh     :: Bool
  , selfCmdLine :: Text
  } deriving (Eq,Ord,Show,Generic)

type ProcessTree = Tree PsProcess

data PsType = PsNormal | PsStop | PsLoginSh
  deriving (Eq,Ord,Show,Generic)

newtype AssocProcess = AssocProcess (Seq.Seq (PsType,Text))
  deriving (Eq,Ord,Show,Generic)

instance Newtype AssocProcess

instance Monoid AssocProcess where
   mempty  = memptydefault
   mappend = mappenddefault

type ReplaceShell = Maybe Text

psProcessToAssoc :: ReplaceShell -> PsProcess -> AssocProcess
psProcessToAssoc = (AssocProcess . Seq.singleton) .: psProcessToAssoc'
  where
    psProcessToAssoc' repsh ps
      | isShell ps   = maybe (PsLoginSh,selfCmdLine ps)
                             (\shl -> (PsLoginSh,shl))  repsh
      | isStopped ps = (PsStop  , selfCmdLine ps)
      | otherwise    = (PsNormal, selfCmdLine ps)

assocProcessToFilePathList :: AssocProcess -> [(PsType,FilePath)]
assocProcessToFilePathList = fmap T.unpack <$>. F.toList . unpack

isStopped  :: PsProcess -> Bool
isStopped = (PsS ==) . baseState . state

isShell :: PsProcess -> Bool
isShell =   loginSh
        ||\ Util.isShell . T.unpack . selfCmdLine
        {-||\ seIsSessLeader . extraState . state-}


isTmux :: PsProcess -> Bool
isTmux = T.isInfixOf "tmux" . selfCmdLine

type TmuxLoc   = (Text,ProcessTree)
type TmuxProcs = (Text,[ProcessTree])

psParser :: Parser PsProcess
psParser = do
  self <- dropSpace >> decimal
  paren<- dropSpace >> decimal
  stat <- dropSpace >> psStateParser
  tty  <- dropSpace >> takeTill isSpace
  _    <- dropSpace >> skipWhile (not . isSpace)
  lsh  <- dropSpace >> try (const True <$> char '-') <|> return False
  cmdl <- fmap T.strip takeText
  return $ PsProcess self paren stat tty lsh cmdl

  where dropSpace = takeWhile isSpace

evalEitherList
  :: (Monoid a)
  => [Either a b] -> Either a [b]
evalEitherList lst = eval . partitionEithers $ lst
  where
    eval ([],bs) = Right bs
    eval (as,_ ) = Left . mconcat $ as

getProcessList :: EitherT Text IO [PsProcess]
getProcessList = do
  res <- sh "ps" "ax -O ppid"
  hoistEither . evalEitherList
              . fmap (mapLeft T.pack . parseOnly psParser)
              . tailSafe . T.lines $ res

getProcessTree :: EitherT Text IO (Tree PsProcess)
getProcessTree = toProcessTree <$> getProcessList

splitPs :: ProcessID -> [PsProcess] -> ([PsProcess], [PsProcess])
splitPs p = L.partition ((==p) . parentPID)

-- ATTN: assuming first line is always PID 1
-- is this always true? It is for Linux and OpenBSD
-- TODO: implement decent algorithm, instead of this crap
toProcessTree :: [PsProcess] -> Tree PsProcess
toProcessTree [] = error "No processes found?"
toProcessTree (fs:lst) = let
  (cp,oth) = splitPs (selfPID fs) lst
  forest' = fmap (flip Node []) cp
  forest  = fmap (fst . flip proc oth) forest'
  in Node fs forest

proc
  :: Tree PsProcess
  -> [PsProcess]
  -> (Tree PsProcess, [PsProcess])
proc tr []  = (tr,[])
proc tr lst = let
  (cp,oth) = splitPs (selfPID . rootLabel $ tr) lst
  forest' = fmap (flip Node []) cp
  (forest,remn) = foldr (\x (f,r) -> (\(y,r') -> (y:f,r')) $ proc x r )
                        ([],oth) forest'
  in (Node (rootLabel tr) forest,remn)

-- | Find tmux server processes
-- in Linux/OpenBSD, its always a child of PID 1
findTmuxServers :: ProcessTree -> [TmuxLoc]
findTmuxServers (Node _ pss)
  = fmap (\pt -> (selfCmdLine . rootLabel $ pt, pt))
  . filter (isTmux . rootLabel) $ pss

-- | Return server containing given PID
identifyMyTmux :: ProcessID -> [TmuxLoc] -> TmuxLoc
identifyMyTmux ppid
  = fromMaybe (error "current process not running inside a tmux session")
  . L.find (\(_,pt) -> Tree.treeAny ((==ppid) . selfPID) pt )

-- | Get list of process trees under tmux server
-- filters out: uninterruptible sleep, paging, dead and defunct
getChildProcesses :: TmuxLoc -> TmuxProcs
getChildProcesses (tmxCmd, Node _ ps) =
  (tmxCmd, mapMaybe (Tree.filterPruneTree notDead) ps)

getTmuxProcs :: EitherT Text IO TmuxProcs
getTmuxProcs = do
  pid <- liftIO getProcessID
  getChildProcesses . identifyMyTmux pid . findTmuxServers <$> getProcessTree

-- | Is a given process id in the process tree?
processIdInTmuxProcs :: ProcessID -> [ProcessTree] -> Bool
processIdInTmuxProcs pid = any (Tree.treeAny ((== pid) . selfPID))

-- | Translate process id into list of commands
-- TODO: consider replace shell option
getProcessIdCommands :: ProcessID -> [ProcessTree] -> [(PsType,FilePath)]
getProcessIdCommands pid pt = let
  candidate = L.find (Tree.treeAny ((== pid) . selfPID)) pt
  in case candidate of
    Nothing -> []
    Just tr -> assocProcessToFilePathList
             . F.foldMap (psProcessToAssoc Nothing)
             $ tr




-------------------------------------------------------------------------------
-- Debug functions ------------------------------------------------------------
-------------------------------------------------------------------------------
printTmuxTree :: IO ()
printTmuxTree = do
  procTree <- runEitherT getProcessTree
  pid      <- getProcessID
  case procTree of
    Left err -> putStrLn . T.unpack $ err
    Right pt -> printTmuxChilds
              . getChildProcesses
              . identifyMyTmux pid
              . findTmuxServers
              $ pt

printTmuxChilds :: TmuxProcs -> IO ()
printTmuxChilds (tmxCmd,forst) = do
  putStrLn . T.unpack $ tmxCmd
  mapM_ (putStrLn . showPsTree) forst

printProcessTree :: IO ()
printProcessTree = do
  pt <- runEitherT getProcessTree
  case pt of
    Left err -> putStrLn . T.unpack $ err
    Right tr -> putStrLn . showPsTree $ tr

showPsTree :: Tree PsProcess -> String
showPsTree = drawTree . fmap (`concatWith` [ show . selfPID
                                           , show . loginSh
                                           , show . selfCmdLine])
  where concatWith v = mconcat . mapV v
