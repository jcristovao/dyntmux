{-# LANGUAGE OverloadedStrings #-}

module YamlError
  ( YamlError(..)
  , parseYaml
  ) where

import Data.Monoid
import Data.Maybe
import Data.List as L
import Data.Yaml
import Text.Libyaml

import Data.Attoparsec.Text
import Control.Applicative

import Data.ByteString as BS
import Data.ByteString.Char8 as BSC

import Data.Text (Text)
import qualified Data.Text as T
import Text.Printf

nonEmpty :: (Monoid a, Eq a) => [a] -> [a]
nonEmpty = L.filter (/= mempty)

isLeft :: Either a b -> Bool
isLeft = either (const True) (const False)

partial :: ByteString -> Int
partial fl = let
  sublist = L.zip [1..] . L.map BSC.unlines . nonEmpty . L.inits . BSC.lines $ fl
  in fst
   . fromMaybe (1,BS.empty)
   . L.find (\(_,txt) -> isLeft (decodeEither' txt :: Either ParseException Object))
   $ sublist

data YamlError  = YamlError
  { errorMsg    :: String
  , errorLine   :: Maybe Int
  , errorCol    :: Maybe Int
  } deriving (Eq)

instance Show YamlError where
  show (YamlError err Nothing _)
    = "[ERROR] Line: 1 " ++ err
  show (YamlError err (Just ln) Nothing)
    = "[ERROR] Line: "  ++ show ln ++ " " ++ err
  show (YamlError err (Just ln) (Just col))
    = "[ERROR] Line: " ++ show ln ++ " Column: " ++ show col ++ " " ++ err

evaluateYamlException :: YamlException -> ByteString -> YamlError
evaluateYamlException yExcept bs = case yExcept of
  YamlException err -> YamlError err (Just . partial $ bs) Nothing
  YamlParseException prob contx (YamlMark _ ln col)
    -> YamlError (prob ++ "(" ++ contx ++ ")") (Just ln) (Just col)

evaluateYamlEvent :: Maybe Event -> Maybe Event -> ByteString -> YamlError
evaluateYamlEvent Nothing (Just EventStreamStart) _
  = YamlError "Is the yaml file empty?"
              (Just 1) (Just 1)
evaluateYamlEvent (Just EventStreamEnd) (Just EventDocumentStart) _
  = YamlError "Is the yaml file empty?"
              (Just 1) (Just 1)
evaluateYamlEvent recvd expctd bs
  = YamlError (printf "Expected %s, received %s in Yaml"
               (maybe "NULL" show expctd)
               (maybe "NULL" show recvd))
              (Just . partial $ bs) Nothing

jsonErrToHumanErr :: Text -> String
jsonErrToHumanErr err = either (const "Internal parsing error")  T.unpack
  $ flip parseOnly err
  $  (string "when expecting a HashMap Text a, encountered String instead"
      >> return "Invalid key, must have the format \"key: value\"")
  <|>(string "could not find expected ':' in while scanning a simple key"
      >> return "Missing ':' in key")
  <|> takeText

evaluateJsonException :: String -> ByteString -> YamlError
evaluateJsonException err bs = let
  err' = jsonErrToHumanErr (T.pack err)
  in YamlError err' (Just . partial $ bs) Nothing

parseYaml :: ByteString -> Either YamlError Object
parseYaml bs = let
  pRes = decodeEither' bs :: Either ParseException Object
  yErr err = YamlError err (Just . partial $ bs) Nothing
  in case pRes of
    Left excp -> Left $ case excp of
      NonScalarKey
                -> yErr "Non Scalar Key found in Yaml"
      UnknownAlias alias
                -> yErr $ printf "Unknown alias %s Yaml" alias
      UnexpectedEvent recvd expctd
                -> evaluateYamlEvent recvd expctd bs
      InvalidYaml Nothing
                -> yErr "Invalid Yaml"
      InvalidYaml (Just yExcept)
                -> evaluateYamlException yExcept bs
      AesonException err
                -> evaluateJsonException err bs
      OtherParseException othE
                -> yErr (show othE)

    Right yaml -> Right yaml

