#ifndef TERMINALSIZE_H
#define TERMINALSIZE_H

typedef struct {
	int cols;
	int lins;
} term_size;


int terminal_lines (void);
int terminal_cols  (void);
int terminal_size (term_size* res);

#endif /* TERMINALSIZE_H */
