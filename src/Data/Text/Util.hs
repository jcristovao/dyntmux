{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE ScopedTypeVariables #-}

module Data.Text.Util
( -- Text Utils
  (.++)
, (.<>)
, (++.)
, (<>.)
, (<>)
, showStr
, tshow
, pontuationToSpaces
, filterAlphaNum
, zapWords
, l2s
) where

-------------------------------------------------------------------------------
-- Imports --------------------------------------------------------------------
-------------------------------------------------------------------------------
import Data.Monoid ((<>))
import Data.Char
import Data.String
import Data.Text (Text)
import qualified Data.Text as T
import qualified Data.Text.Lazy as LT


------------------------------------------------------------------------------
-- Text Utils ----------------------------------------------------------------
------------------------------------------------------------------------------
-- | Infix operator to join @String@ to @Text@, resulting in @Text@.
infixr 5 .++
infixr 5 .<>
(.++), (.<>) :: String -> Text -> Text
a .++ b = T.append (T.pack a) b
(.<>) = (.++)

-- | Infix operator to join @Text@ to @String@, resulting in @Text@.
--
-- /Note:/ @(++.) = flip (.++)@
infixr 5 ++.
infixr 5 <>.
(++.), (<>.) :: Text -> String -> Text
a ++. b = T.append a (T.pack b)
(<>.) = (++.)

-- | Show a string like value (@String@ or @Text@) without the annoying quotes
showStr :: (IsString s, Show s) => s -> String
showStr = Prelude.init . Prelude.tail . show

-- | Show as text
tshow :: (Show s) => s -> Text
tshow = T.pack . show

-- | Strip all non alphanumeric caracters to spaces
pontuationToSpaces :: Text -> Text
pontuationToSpaces = T.map (\c -> if not (isAlphaNum c) then ' ' else c)

-- | Strip all non alphanumeric caracters to spaces, and colapse multiple spaces into one
-- TODO: perhaps a well thought out fold is more effective
filterAlphaNum :: Text -> Text
filterAlphaNum = T.unwords . filter (not . T.null)
               . T.split (==' ') . pontuationToSpaces

-- eliminate a list of words (or texts) from a text
-- TODO: perhaps a well thought out fold is more effective
zapWords :: [Text] -> Text -> Text
zapWords lst phrase = foldl (\phr w -> if T.null w then phr else T.replace w "" phr) phrase lst

-- Lazy to strict
l2s :: LT.Text -> T.Text
l2s = T.concat . LT.toChunks
