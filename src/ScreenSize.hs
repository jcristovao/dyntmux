{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE CPP #-}
module ScreenSize where

import Control.Monad.Trans.Either
import Control.Monad.Trans.Either.Exception
import Control.Exception
import Tmux.Types
import TerminalSize

detectScreenSize :: EitherT IOException IO ScreenSize
detectScreenSize = toScreenSize . eIoTry $ getTerminalSize
  where toScreenSize = fmap (\(x,y) -> (fromIntegral x, fromIntegral y - 1))

-- | Get current terminal size, or return the provided default if the system
-- call failed.
getScreenSize :: ScreenSize -> IO ScreenSize
getScreenSize defSs = eitherT (const . return $ defSs) return detectScreenSize

