#include <sys/ioctl.h>
#include <stdio.h>
#include <unistd.h>

#include "../include/terminalsize.h"

/** Agressively try to determine the terminal size
 * try stdout, stderr and stdin (in this order). */
int terminal_size (term_size* res) {
	int ret_stdin  = 0;
	int ret_stdout = 0;
	int ret_stderr = 0;
	struct winsize w;
	ret_stdout = ioctl(STDOUT_FILENO,TIOCGWINSZ,&w);
	if (-1 != ret_stdout) {
		res->cols = (short) w.ws_col;
		res->lins = (short) w.ws_row;
		return 0;
	}
	ret_stderr = ioctl(STDERR_FILENO,TIOCGWINSZ,&w);
	if (-1 != ret_stderr) {
		res->cols = (short) w.ws_col;
		res->lins = (short) w.ws_row;
		return 0;
	}

	ret_stdin  = ioctl(STDIN_FILENO ,TIOCGWINSZ,&w);
	if (-1 != ret_stdin) {
		return -1;
	} else {
		res->cols = (short) w.ws_col;
		res->lins = (short) w.ws_row;
		return 0;
	}

	return -1;
}
