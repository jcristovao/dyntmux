{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE FlexibleInstances #-}

import Prelude hiding (putStrLn)
import Data.Maybe

import Control.Applicative
import Control.Monad
import Control.Monad.Trans.Reader
import Control.Monad.Trans.Either
{-import Control.Monad.IO.Class-}

import Data.Easy
import Data.AltComposition
import Data.String
import Data.Text (Text)
import qualified Data.Text as T
import Data.Text.IO
import Data.Text.Util
import Data.Default.Generics

import qualified Paths_dyntmux as Paths (version)
import Data.Version (showVersion)
import System.IO (hIsTerminalDevice,stdout)
import System.Exit
import System.Lifted

import Options.Applicative (execParser)
import Tmux.Classes
import Tmux.Types
import Tmux.Project.Types
import Tmux.Configuration
import Tmux.Project
import Tmux.Interface
import Tmux.CmdLine
import Tmux.Util
import Tmux.Folders

{-import ScreenSize-}
{-import Debug.Trace-}

notDebug :: MainOpts -> Bool
notDebug = not . optDebug

projectToTmux :: Project -> Reader Env ([Text], Text)
projectToTmux proj = do
  sessions <- projectToSessions proj
  local ( \env -> env { defaultPath = Just . _path . _projRoot $ proj})
       $ sessionsToTmux sessions

updateTmuxCmd :: Project -> Env -> Env
updateTmuxCmd proj env =
  maybe env ((`setTmuxPath` env) . _tCommand) . _projTmuxCommand $ proj

diagEitherT :: (IsString e, Show e) => Text -> EitherT e IO a -> IO a
diagEitherT what eit = runEitherT eit >>= \ei -> case ei of
  Left  e -> putStrLn (what <> ": " <>. showStr e) >> exitFailure
  Right p -> return p

getShellEnv :: Report -> Colour -> Lenient -> FilePath -> IO ShellEnv
getShellEnv report colour lenient tmuxPth = do
  shellEnv' <- runEitherT $ analyseEnv report colour lenient tmuxPth
  case shellEnv' of
    Left  rep   -> unless (_report report) (putStrLn rep)
                >> exitFailure
    Right shEnv -> return shEnv

tm :: Env -> Text -> IO Text
tm env cmd = diagEitherT cmd  . runTmux env . tmux' $ cmd

------------------------------------------------------------------------------
-- Main ----------------------------------------------------------------------
------------------------------------------------------------------------------
main :: IO ()
main = do

  -- process command line options -----------------------------------
  -------------------------------------------------------------------
  mainopts <- execParser mainOpts

  -- report version -------------------------------------------------
  -------------------------------------------------------------------
  when (optVersion mainopts) $ do
    putStrLn $ "dyntmux " ++. showVersion Paths.version
    exitSuccess

  -- report no command found ----------------------------------------
  -------------------------------------------------------------------
  when (isNothing . optCommand $ mainopts) $ do
    -- TODO: fix this... damn optparse-applicative changes interface
    -- without any explanation
    {-putStrLn $ usageT "No valid combination of switches and/or commands"-}
    exitFailure
  let command = fromJustNote "Internal error in optCommand" . optCommand
              $ mainopts

  -- Analyse environment, and report if something is wrong ----------
  -------------------------------------------------------------------
  -- Colour output?
  colour'' <- hIsTerminalDevice stdout
  let colour'= not (optColour mainopts)
      colour = Colour (colour'' && colour')

  -- TODO: tmux version is analysed based on findexecutable, when configuration
  -- file may specify correct one. On the other hand, save operations do not
  -- have a 'a priori' configuration file (that would be an update operation)
  let  report  = Report (command == Doctor)
       lenient = Lenient $ case command of Save _ -> False
                                           Doctor -> False
                                           _      -> True
  shellEnv <- getShellEnv report colour lenient "tmux"

  -- Finally, get the screen size -----------------------------------
  -------------------------------------------------------------------
  {-sz      <- getScreenSize (80,25)-}
  let sz = (80,25)

  -- Configuration file ---------------------------------------------
  -------------------------------------------------------------------
  configurationFn <- getConfigFileName
  configuration   <- loadConfiguration configurationFn
  when (_report report) (do

    let ok = _colour colour ?. okT .$ T.pack
    putStrLn $ noT "Configuration:" <> ok configurationFn <> noT ""
    exitSuccess
    )

  -- Now, setup the environement ------------------------------------
  -------------------------------------------------------------------
  let env = def { screenSize    = sz
                , colourOutput  = colour
                , sys           = shellEnv
                , configur      = configuration
                }

  -- Command execution ----------------------------------------------
  -------------------------------------------------------------------
  case command of

    Load loadOpts   -> do
      projLocation <- initProjLoc   . loadOptsToProjLocation $ loadOpts
      projfile     <- reportEitherT . projFile projLocation . loadProj $ loadOpts
      proj         <- reportEitherT $ loadProject projfile

      -- Get effective tmux to use from project file
      let updEnv'             = updateTmuxCmd proj env

      -- Recheck environment (due to possible tmux name/loc update)
      -- i.e., confirm given tmux location (on project)
      shellEnv' <- getShellEnv (Report False) colour (Lenient False)
                               (tmuxPath updEnv')
      let updEnv = setTmuxPath (shellTmux shellEnv') updEnv'
          toCmd  = ((tmuxPath updEnv .<> " " <> tmuxOptsRend <> " ") <>)
          addOpts tl   = (tmuxOptsRend <> " " <> tl)
          tmuxOptsCmdl = fromMaybe def . optTmux $ mainopts
          tmuxOptsProj = fromMaybe def . _projTmuxOptions $ proj
          -- monoid instance works as First for strings, any for booleans
          tmuxOpts      = tmuxOptsCmdl <> tmuxOptsProj
          tmuxOptsRend  = fst . runRdr def . toTmux $ tmuxOpts


      -- TODO: should use tmuxOpts to determine correct base-index
      -- create empty session just to get information
      {-tm updEnv' (addOpts "new-session")-}
      {-baseI <- parseTmuxNumeric "base-index"-}
           {-<$> tm updEnv (addOpts "show-options -g base-index")-}
      {-print baseI-}
      {-basePaneI <- parseTmuxNumeric "pane-base-index"-}
           {-<$> tm updEnv (addOpts "show-options -g pane-base-index")-}
      {-print basePaneI-}
      {-tm updEnv' (addOpts "send-keys C-d")-}


      -- project to tmux commands,
      -- also, get the tmux command from project
      let (tmuxLines, attach) = runRdr env . projectToTmux $ proj
      -- Recheck environment (due to possible tmux name/loc update)
      -- TODO: this needs to move up: when we start reading the base index
      -- this will influence the projectToTmux call for sure, as the
      -- base indexes will have to be taken into account for the select-pane

      -- now do the load stuff
      if notDebug mainopts
        then do
          mapM_ (\tl -> diagEitherT (toCmd tl) . runTmux updEnv . tmux'
                      $ addOpts tl)
                tmuxLines
          void . runTmux updEnv . tshdo $ addOpts attach
        else do
          mapM_ (putStrLn . toCmd) tmuxLines
          putStrLn . toCmd $ attach


    Save saveOpts  -> do
      projLocation <- initProjLoc . saveOptsToProjLocation $ saveOpts
      projBase     <- chooseBaseFolder (saveBase saveOpts)
      let projfile = saveProjName saveOpts
      status <- runEitherT
              $ runRdrT (env { defaultPath = Just projBase })
              $ do
                  -- ReaderT Env (Either Text IO) Project
                  proj <- saveCurrentSessionToProject
                  -- ReaderT Env (EitherT Text IO) ()
                  saveProject Nothing projLocation projfile proj
      when (isLeft status) (putStrLn . (\(Left err) -> err) $ status)

    List listOpts ->  initProjLoc . listOptsToProjLocation § listOpts
                  >>= runRdrT env . listProjects

    Edit editOpts   -> do
      projLocation <- initProjLoc   $ editOptsToProjLocation editOpts
      projfile     <- reportEitherT $ projFile projLocation (editProj editOpts)
      let editor = shellEnvEditor . sys $ env
      void . runRdrT editor . tshdo . T.pack $ projfile

    Nator opts -> do
      let ncomm
           | natorImplode opts = do
              putStrLn "Are you sure you want to delete all tmuxinator configs?"
              conf <- confirm
              when conf (wipeProjects LocatNator)
           | otherwise = initProjLoc LocatNator >>= runRdrT env . listProjects
      ncomm

    _ -> do
      -- TODO : fix this
      {-putStrLn $ usageT "Unexpected command"-}
      exitFailure


