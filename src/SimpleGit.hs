{-# LANGUAGE OverloadedStrings #-}
module SimpleGit
  ( getGitBase
  , getGitFolder
  ) where

import Foreign.C
import Bindings.Libgit2.Repository
import Bindings.Libgit2.Threads
import Control.Exception
import Control.Monad.Trans.Either
import System.FilePath
import System.Util

-- This value is for Linux... for Windows (or BSD) it is smaller
-- but since this is a maximum buffer size, no harm can be done, right?
{-# ANN max_path ("HLint: ignore Use camelCase"::String) #-}
max_path :: Int
max_path = 4096

{-# ANN max_path' ("HLint: ignore Use camelCase"::String) #-}
max_path' :: CSize
max_path' = CSize (fromIntegral max_path)

-- | Get .git folder, if it exists
getGitFolder :: String -> EitherT IOException IO String
getGitFolder currDir = EitherT $
  bracket c'git_threads_init
  (const   c'git_threads_shutdown)
  (\libst -> if libst /= 0
                then retIOErr "Error initializing GIT library"
                else withCString (replicate max_path '*') $ \res  ->
            (withCString currDir $ \path ->
             (withCString ""      $ \empt -> do
              status <- c'git_repository_discover res max_path' path (CInt 0) empt
              if status == 0
                then fmap Right $ peekCString res
                else retIOErr "No GIT repository found!"
            ))
  )
  where
    retIOErr = return . Left . userError

-- | Get folder where .git is, if applicable
getGitBase :: String -> EitherT IOException IO String
getGitBase currDir = do
  base <- fmap (takeDirectory . takeDirectory) $ getGitFolder currDir
  isRW base
