{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE ForeignFunctionInterface #-}
{-# OPTIONS_GHC -fno-warn-orphans #-}
{- |
Module      : $Header$
Description : Screen Size IOCtl
Copyright   : João Cristóvão
License     : BSD3

Stability   : none
Portability : POSIX

This modules implements an IOCtl to get the screen size, as described in:
http://stackoverflow.com/a/1022961/516184
-}
module TerminalSize (getTerminalSize) where

import Foreign
{-import Foreign.Ptr-}
import Foreign.C
import Foreign.C.Types

#include "terminalsize.h"

type TerminalSizeRes = (Int32,Int32)

instance Storable TerminalSizeRes where
  sizeOf    _ = (#size term_size)
  alignment _ = alignment (undefined :: Int)
  peek ptr = do
    x <- (#peek term_size,cols) ptr
    y <- (#peek term_size,lins) ptr
    return $ (x,y)

foreign import ccall unsafe "include/terminalsize.h terminal_size"
  cterminal_size :: Ptr TerminalSizeRes -> IO CInt

-- | Returns the terminal size. Tries STDOUT, STDERR and STDIN in order.
getTerminalSize :: IO (Int64,Int64)
getTerminalSize = do
  alloca $ \termSizePtr -> do
    throwErrnoIfMinus1_ "Cannot determine screensize through IOCtl"
      $ cterminal_size termSizePtr
    (x,y) <- peek termSizePtr
    return (fromIntegral x,fromIntegral y)

