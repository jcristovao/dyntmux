#!/bin/bash
pushd .
cd src/Tmux/Internal
sed "s/WHAT/Session/g" Process.hst > ProcessSession.hs
sed "s/WHAT/Window/g"  Process.hst > ProcessWindow.hs
sed "s/WHAT/Pane/g"    Process.hst > ProcessPane.hs
popd
cabal configure --enable-tests
cabal build

