{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE FlexibleInstances #-}
{-# OPTIONS_GHC -fno-warn-orphans #-}

module SimpleGitSpec (spec) where

{-import Control.Applicative-}
import Control.Monad
import Control.Monad.Trans.Either

{-import qualified Data.Map as Map-}
import Test.Hspec
import Test.QuickCheck.Instances()

import Data.Text (Text)
import qualified Data.List as L
import Data.Easy
import Tmux.Folders
import System.IO.Error
import System.IO
import System.Directory
import System.FilePath ((</>))
import System.Util
import SimpleGit

isLeftAnd :: (a -> Bool) -> Either a b -> Bool
isLeftAnd f ei = case ei of
  Left v -> f v
  _      -> False

isRightAnd :: (b -> Bool) -> Either a b -> Bool
isRightAnd f ei = case ei of
  Right v -> f v
  _      -> False

shouldSatisfyIO, ssIO :: Show a => IO a -> (a -> Bool) -> Expectation
shouldSatisfyIO action f = action >>= flip shouldSatisfy f
ssIO = shouldSatisfyIO

createEmptyFile :: String -> IO ()
createEmptyFile fn = do
  handl <- openFile fn WriteMode
  hPutStrLn handl ""
  hClose handl

{-# ANN spec ("HLint: ignore Redundant do"::String) #-}
spec :: Spec
spec = describe "SimpleGit" $ do
  it "gets current .git folder" $ do
    (runEitherT . getGitFolder $ ".") `ssIO` (isRightAnd (L.isSuffixOf ".git/"))

  it "returns left value for .." $ do
    (runEitherT . getGitFolder $ "..") `ssIO` isLeft

  it "gets current folder as git base" $ do
    (runEitherT . getGitBase $ ".") `ssIO` (isRightAnd (L.isSuffixOf "dyntmux"))

  it "returns left value for git base of .." $ do
    (runEitherT . getGitBase $ "..") `ssIO` isLeft



