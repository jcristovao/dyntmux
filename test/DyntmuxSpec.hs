{-# LANGUAGE OverloadedStrings #-}

module DyntmuxSpec (spec) where

import Control.Applicative
import Control.Monad
import Control.Monad.Trans.Either
import Control.Monad.Trans.Reader
import Test.Hspec

import Tmux.Classes
import Tmux.Types
import Tmux.Folders
import Tmux.Interface

import ScreenSize
import System.Exit
import Data.Easy

runTmux' :: ReaderT FilePath (EitherT e m) a -> m (Either e a)
runTmux' = runEitherT . runRdrT "tmux"

spec :: Spec
spec = describe "Dyn(namic) Tmux" $ do


  it "Tmux is loaded" $ do
    tmuxLoaded <- isRight <$> runTmux' (tmux' "list-clients")
      {-sh $ do-}
      {-_ <- errExit False $ tmux "list-clients"-}
      {-fmap (==0) lastExitCode-}
    tmuxLoaded `shouldBe` True
  it  "Can load sessions" $ do
    sessions <- runTmux'  getSessions
    sessions `shouldSatisfy` isRight
  {-it "Can generate a valid tmuxinator yaml file" $ do-}
    {-let colour = Colour False-}
        {-report = Report True-}
    {-sz      <- getScreenSize False-}
    {-shellEnv<- runEitherT $ analyseEnv report colour "tmux"-}
    {-when (isLeft shellEnv) exitFailure-}
    {-let env = Env sz colour (fromRight (error "shellEnv inconsistency") shellEnv) Nothing Nothing-}
    {-projLocat <- initProj LocatSmart-}
    {-print projLocat-}
    {-success <- runEitherT $ runRdrT env $ saveCurrentToTmuxinatorYaml projLocat Nothing-}
    {-success `shouldSatisfy` isRight-}


