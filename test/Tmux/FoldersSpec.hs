{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE FlexibleInstances #-}
{-# OPTIONS_GHC -fno-warn-orphans #-}

module Tmux.FoldersSpec (spec) where

{-import Control.Applicative-}
import Control.Monad
import Control.Monad.Trans.Either

{-import qualified Data.Map as Map-}
import Test.Hspec
import Test.QuickCheck.Instances()

import Data.Text (Text)
import qualified Data.List as L
import Data.Easy
import Tmux.Folders
import System.IO.Error
import System.IO
import System.Directory
import System.FilePath ((</>))
import System.Util

data TextToX = X Text deriving (Eq,Ord,Show)
data IntToY  = Y Int deriving  (Eq,Ord,Show)
data BoolToB = B Bool deriving (Eq,Ord,Show)

tf1 :: FilePath
tf1 = "testfile1.txt"

isLeftAnd :: (a -> Bool) -> Either a b -> Bool
isLeftAnd f ei = case ei of
  Left v -> f v
  _      -> False

isRightAnd :: (b -> Bool) -> Either a b -> Bool
isRightAnd f ei = case ei of
  Right v -> f v
  _      -> False

shouldSatisfyIO, ssIO :: Show a => IO a -> (a -> Bool) -> Expectation
shouldSatisfyIO action f = action >>= flip shouldSatisfy f
ssIO = shouldSatisfyIO

tupleCheck :: (a -> Bool,b -> Bool) -> (a,b) -> Bool
tupleCheck (f,g) (a,b) = f a && g b

tupleOr :: (a -> Bool, b -> Bool) -> (a -> Bool, b -> Bool)
        -> (a -> Bool, b -> Bool)
tupleOr (f,g) (h,i) = (f ||\ h, g ||\ i)

createEmptyFile :: String -> IO ()
createEmptyFile fn = do
  handl <- openFile fn WriteMode
  hPutStrLn handl ""
  hClose handl

{-# ANN spec ("HLint: ignore Redundant do"::String) #-}
spec :: Spec
spec = describe "Tmux.Folders" $ do
  it "gets tmuxinator folder" $ do
    runEitherT getNatorFolder `ssIO` (isRightAnd (L.isSuffixOf ".tmuxinator")
                                  ||\ isLeftAnd isDoesNotExistError)

  it "chooseDefProjFolder LocatGit" $ do
    chooseDefProjFolder LocatGit
      `ssIO` tupleCheck ((==LocatGit),L.isSuffixOf "/dyntmux")

  it "chooseDefProjFolder LocalSmart" $ do
    chooseDefProjFolder LocatSmart
      `ssIO` tupleCheck ( ((==LocatGit   ),L.isSuffixOf "/dyntmux")
                `tupleOr` ((==LocatConfig),L.isSuffixOf ".config" )
                `tupleOr` ((==LocatHome  ),L.isPrefixOf "/home"   )
                `tupleOr` ((==LocatCurr  ),L.isSuffixOf "/dyntmux")
                `tupleOr` ((==LocatNator ),L.isSuffixOf ".tmuxinator")
                )

  it "chooseDefProjFolder LocatConfig" $ do
    chooseDefProjFolder LocatConfig
      `ssIO` tupleCheck ( ((==LocatConfig   ), L.isSuffixOf ".config")
                `tupleOr` ((==LocatHome     ), L.isPrefixOf "/home"  )
                )

  it "chooseDefProjFolder LocatHome" $ do
    chooseDefProjFolder LocatHome
      `ssIO` tupleCheck ((==LocatHome),L.isPrefixOf "/home")

  it "chooseDefProjFolder LocatNator" $ do
    chooseDefProjFolder LocatNator
      `ssIO` tupleCheck ((==LocatNator),L.isSuffixOf ".tmuxinator")

  it "chooseBaseFolder with no parameter chooses git" $ do
    chooseBaseFolder Nothing `ssIO`(L.isSuffixOf "/dyntmux"
                                ||\ (== "."))

  it "chooseBaseFolder returns a valid custom folder" $ do
    chooseBaseFolder (Just "test") `ssIO` (=="test")

  it "chooseDefProjFolder LocatConfig" $ do
    chooseDefProjFolder LocatConfig
      `ssIO` tupleCheck (((==LocatConfig),L.isSuffixOf ".config")
                `tupleOr`((==LocatHome  ),L.isPrefixOf "/home"  ))

  it "initProjLoc LocatSmart chooses ./.dyntmux" $ do
    initProjLoc LocatSmart `ssIO` L.isSuffixOf "/.dyntmux"

  it "initProjLoc LocatHome chooses $HOME" $ do
    initProjLoc LocatHome `ssIO` L.isPrefixOf "/home"

  it "initProjLoc LocatCurr chooses current folder" $ do
    initProjLoc LocatCurr `ssIO` L.isSuffixOf "/dyntmux"

  it "projFile with a specified configuration file returns Left if it does not exist" $
    runEitherT ( projFile "." (Just "fefefefeege.yml"))
      `ssIO` isLeft

  before (createEmptyFile tf1) $ after (removeFile tf1) $ do
    it "projFile with a specified (existing) configuration file returns Right" $
      runEitherT (projFile "." (Just tf1)) `ssIO` isRightAnd (=="./" ++ tf1)

  let folder   = ".dyntmux2"
      filename = folder </> (tf1 ++ ".yml")
  before (do
    void . runEitherT $ checkOrCreate folder
    createEmptyFile filename)
    $ after (do
      removeFile filename
      removeDirectory folder)
    $ do
      it "projFile without a configuration file returns existing yaml" $
        runEitherT (projFile folder Nothing) `ssIO` isRightAnd (==filename)

