{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE FlexibleInstances #-}

module Tmux.DynProjSpec (spec) where

import Test.Hspec

import Data.Function
import Data.Attoparsec.Text.Parsec
import Data.Text (Text)
import Tmux.Classes
import Tmux.Layout

-- all conditions must be true
allCond' :: a -> a -> [a -> a -> Bool] -> Bool
allCond' _ _ [] = False
allCond' a b conds = foldr (\f acc -> acc && f a b) True conds

fTup :: (a -> a -> b) -> (a,a) -> b
fTup f (x,y) = f x y

similarTuple :: Similar a => (a,a) -> Bool
similarTuple = fTup (==~)

class Similar a where
  (==~) :: a -> a -> Bool

instance Similar LayoutContents where
  (==~) t0 t1 = case t0 of
    LCPaneId _ -> case t1 of
      LCPaneId _ -> True
      _          -> False
    LCVertSplit lus -> case t1 of
      LCVertSplit lus' -> lus ==~ lus'
      _                -> False
    LCHorzSplit lus -> case t1 of
      LCHorzSplit lus' -> lus ==~ lus'
      _                -> False

instance Similar LayoutUnit where
  (==~) l0 l1 = allCond' l0 l1
                  [ (==) `on` luWidth
                  , (==) `on` luHeight
                  , (==) `on` luX0
                  , (==) `on` luY0
                  , (==~) `on` luContents ]

instance Similar [LayoutUnit] where
  (==~) l0s l1s
    =    (length l0s == length l1s)
      && all similarTuple (zip l0s l1s)

toLayoutUnits :: Text -> [LayoutUnit]
toLayoutUnits = either (const []) id . parseOnly parseL

spec :: Spec
spec = describe "Tmuxinator" $ do
  {-it "Writes human readable yaml file" $ do-}
    {-let yaml = TmuxinatorYaml-}
                {-(Project "0")-}
                {-Nothing-}
                {-(Just (Socket "./asf"))-}
                {-(Just (Pre "ls"))-}
                {-(Just (PreW "ls -lA"))-}
                {-(Just (TOptions "-2C -c \"ls -lA\" new-session -A" ))-}
                {-(Just (TCommand "tmux"))-}
               {-[ SW (Map.fromList [("1",[Command "0"])])-}
               {-, MW (Map.fromList-}
                  {-[(  "2",-}
                      {-Panes defaultLayout-}
                      {-[PCommands-}
                      {-(Map.singleton "test" [Command "0", Command "git 10"])-}
                        {-]-}
                    {-)])-}
               {-]-}
    {-writeHumanReadableYaml "test.yml" yaml-}
    {-tmuxinator <- yamlToTmuxinator yaml-}
    {-runEitherT (parseTmuxinator "test.yml")-}
      {-`shouldReturn` Right tmuxinator-}

  it "Calculates even horizontal layout (2 panes)" $ do
    let x = Layout 0 LayoutEvenHorizontal []
        l = fillLayoutUnits x 2 0 (273,69)
        str = "273x69,0,0{136x69,0,0,160,136x69,137,0,161}"
    (toLayoutUnits str, layoutUnits l) `shouldSatisfy` similarTuple

  it "Calculates even horizontal layout (3 panes)" $ do
    let x = Layout 0 LayoutEvenHorizontal []
        l = fillLayoutUnits x 3 0 (273,69)
        str = "273x69,0,0{90x69,0,0,160,90x69,91,0,217,91x69,182,0,161}"
    (toLayoutUnits str, layoutUnits l) `shouldSatisfy` similarTuple

  it "Calculates even horizontal layout (4 panes)" $ do
    let x = Layout 0 LayoutEvenHorizontal []
        l = fillLayoutUnits x 4 0 (273,69)
        str = "273x69,0,0{67x69,0,0,160,67x69,68,0,220,67x69,136,0,221,69x69,204,0,161}"
    (toLayoutUnits str, layoutUnits l) `shouldSatisfy` similarTuple

  it "Calculates even horizontal layout (19 panes)" $ do
    let x = Layout 0 LayoutEvenHorizontal []
        l = fillLayoutUnits x 19 0 (273,69)
        str = "273x69,0,0{13x69,0,0,3,13x69,14,0,15,13x69,28,0,16,13x69,42,0,17,13x69,56,0,18,13x69,70,0,19,13x69,84,0,20,13x69,98,0,21,13x69,112,0,22,13x69,126,0,23,13x69,140,0,24,13x69,154,0,25,13x69,168,0,8,13x69,182,0,12,13x69,196,0,13,13x69,210,0,14,13x69,224,0,5,13x69,238,0,6,21x69,252,0,7}"
    (toLayoutUnits str, layoutUnits l) `shouldSatisfy` similarTuple

  it "Calculates even vertical layout (2 panes)" $ do
    let x = Layout 0 LayoutEvenVertical []
        l = fillLayoutUnits x 2 0 (273,69)
        str = "273x69,0,0[273x34,0,0,160,273x34,0,35,161]"
    (toLayoutUnits str, layoutUnits l) `shouldSatisfy` similarTuple

  it "Calculates even vertical layout (3 panes)" $ do
    let x = Layout 0 LayoutEvenVertical []
        l = fillLayoutUnits x 3 0 (273,69)
        str = "273x69,0,0[273x22,0,0,160,273x22,0,23,222,273x23,0,46,161]"
    (toLayoutUnits str, layoutUnits l) `shouldSatisfy` similarTuple

  it "Calculates even horizontal layout (4 panes)" $ do
    let x = Layout 0 LayoutEvenVertical []
        l = fillLayoutUnits x 4 0 (273,69)
        str = "273x69,0,0[273x16,0,0,160,273x16,0,17,222,273x16,0,34,223,273x18,0,51,161]"
    (toLayoutUnits str, layoutUnits l) `shouldSatisfy` similarTuple

  it "Calculates even horizontal layout (19 panes)" $ do
    let x = Layout 0 LayoutEvenVertical []
        l = fillLayoutUnits x 19 0 (273,69)
        str = "273x69,0,0[273x2,0,0,3,273x2,0,3,15,273x2,0,6,16,273x2,0,9,17,273x2,0,12,18,273x2,0,15,19,273x2,0,18,20,273x2,0,21,21,273x2,0,24,22,273x2,0,27,23,273x2,0,30,24,273x2,0,33,25,273x2,0,36,8,273x2,0,39,12,273x2,0,42,13,273x2,0,45,14,273x2,0,48,5,273x2,0,51,6,273x15,0,54,7]"
    (toLayoutUnits str, layoutUnits l) `shouldSatisfy` similarTuple


  it "Calculates main horizontal layout (2 panes)" $ do
    let x = Layout 0 LayoutMainHorizontal []
        l = fillLayoutUnits x 2 0 (273,69)
        str = "273x69,0,0[273x24,0,0,160,273x44,0,25,161]"
    (toLayoutUnits str, layoutUnits l) `shouldSatisfy` similarTuple

  it "Calculates main horizontal layout (3 panes)" $ do
    let x = Layout 0 LayoutMainHorizontal []
        l = fillLayoutUnits x 3 0 (273,69)
        str = "273x69,0,0[273x24,0,0,158,273x44,0,25{136x44,0,25,160,136x44,137,25,161}]"
    (toLayoutUnits str, layoutUnits l) `shouldSatisfy` similarTuple

  it "Calculates main horizontal layout (4 panes)" $ do
    let x = Layout 0 LayoutMainHorizontal []
        l = fillLayoutUnits x 4 0 (273,69)
        str = "273x69,0,0[273x24,0,0,160,273x44,0,25{90x44,0,25,210,90x44,91,25,216,91x44,182,25,161}]"
    (toLayoutUnits str, layoutUnits l) `shouldSatisfy` similarTuple

  it "Calculates main horizontal layout (19 panes)" $ do
    let x = Layout 0 LayoutMainHorizontal []
        l = fillLayoutUnits x 19 0 (273,69)
        str = "273x69,0,0[273x24,0,0,3,273x44,0,25{14x44,0,25,15,14x44,15,25,16,14x44,30,25,17,14x44,45,25,18,14x44,60,25,19,14x44,75,25,20,14x44,90,25,21,14x44,105,25,22,14x44,120,25,23,14x44,135,25,24,14x44,150,25,25,14x44,165,25,8,14x44,180,25,12,14x44,195,25,13,14x44,210,25,14,14x44,225,25,5,14x44,240,25,6,18x44,255,25,7}]"
    (toLayoutUnits str, layoutUnits l) `shouldSatisfy` similarTuple

  it "Calculates main vertical layout (2 panes)" $ do
    let x = Layout 0 LayoutMainVertical []
        l = fillLayoutUnits x 2 0 (273,69)
        str = "273x69,0,0{80x69,0,0,160,192x69,81,0,161}"
    (toLayoutUnits str, layoutUnits l) `shouldSatisfy` similarTuple

  it "Calculates main vertical layout (3 panes)" $ do
    let x = Layout 0 LayoutMainVertical []
        l = fillLayoutUnits x 3 0 (273,69)
        str = "273x69,0,0{80x69,0,0,160,192x69,81,0[192x34,81,0,210,192x34,81,35,161]}"
    (toLayoutUnits str, layoutUnits l) `shouldSatisfy` similarTuple

  it "Calculates main vertical layout (4 panes)" $ do
    let x = Layout 0 LayoutMainVertical []
        l = fillLayoutUnits x 4 0 (273,69)
        str = "273x69,0,0{80x69,0,0,160,192x69,81,0[192x22,81,0,210,192x22,81,23,216,192x23,81,46,161]}"
    (toLayoutUnits str, layoutUnits l) `shouldSatisfy` similarTuple

  it "Calculates main vertical layout (19 panes)" $ do
    let x = Layout 0 LayoutMainVertical []
        l = fillLayoutUnits x 19 0 (273,69)
        str = "273x69,0,0{80x69,0,0,3,192x69,81,0[192x2,81,0,15,192x2,81,3,16,192x2,81,6,17,192x2,81,9,18,192x2,81,12,19,192x2,81,15,20,192x2,81,18,21,192x2,81,21,22,192x2,81,24,23,192x2,81,27,24,192x2,81,30,25,192x2,81,33,8,192x2,81,36,12,192x2,81,39,13,192x2,81,42,14,192x2,81,45,5,192x2,81,48,6,192x18,81,51,7]}"
    (toLayoutUnits str, layoutUnits l) `shouldSatisfy` similarTuple

  it "Calculates tiled layout (2 panes)" $ do
    let x = Layout 0 LayoutTiled []
        l = fillLayoutUnits x 2 0 (273,69)
        str = "273x69,0,0[273x34,0,0,258,273x34,0,35,259]"
    (toLayoutUnits str, layoutUnits l) `shouldSatisfy` similarTuple

  it "Calculates tiled layout (3 panes)" $ do
    let x = Layout 0 LayoutTiled []
        l = fillLayoutUnits x 3 0 (273,69)
        str = "273x69,0,0[273x34,0,0{136x34,0,0,258,136x34,137,0,259},273x34,0,35,261]"
    (toLayoutUnits str, layoutUnits l) `shouldSatisfy` similarTuple

  it "Calculates tiled layout (4 panes)" $ do
    let x = Layout 0 LayoutTiled []
        l = fillLayoutUnits x 4 0 (273,69)
        str = "273x69,0,0[273x34,0,0{136x34,0,0,258,136x34,137,0,259},273x34,0,35{136x34,0,35,261,136x34,137,35,262}]"
    (toLayoutUnits str, layoutUnits l) `shouldSatisfy` similarTuple

  it "Calculates tiled layout (5 panes)" $ do
    let x = Layout 0 LayoutTiled []
        l = fillLayoutUnits x 5 0 (273,69)
        str = "273x68,0,0[273x22,0,0{136x22,0,0,258,136x22,137,0,259},273x22,0,23{136x22,0,23,261,136x22,137,23,262},273x23,0,46,263]"
    (toLayoutUnits str, layoutUnits l) `shouldSatisfy` similarTuple

  it "Calculates tiled layout (6 panes)" $ do
    let x = Layout 0 LayoutTiled []
        l = fillLayoutUnits x 6 0 (273,69)
        str = "273x68,0,0[273x22,0,0{136x22,0,0,3,136x22,137,0,8},273x22,0,23{136x22,0,23,12,136x22,137,23,5},273x23,0,46{136x23,0,46,6,136x23,137,46,7}]"
    (toLayoutUnits str, layoutUnits l) `shouldSatisfy` similarTuple

  it "Calculates tiled layout (7 panes)" $ do
    let x = Layout 0 LayoutTiled []
        l = fillLayoutUnits x 7 0 (273,69)
        str = "272x68,0,0[273x22,0,0{90x22,0,0,3,90x22,91,0,8,91x22,182,0,12},273x22,0,23{90x22,0,23,13,90x22,91,23,5,91x22,182,23,6},273x23,0,46,7]"
    (toLayoutUnits str, layoutUnits l) `shouldSatisfy` similarTuple

  it "Calculates tiled layout (8 panes)" $ do
    let x = Layout 0 LayoutTiled []
        l = fillLayoutUnits x 8 0 (273,69)
        str = "272x68,0,0[273x22,0,0{90x22,0,0,3,90x22,91,0,8,91x22,182,0,12},273x22,0,23{90x22,0,23,13,90x22,91,23,14,91x22,182,23,5},273x23,0,46{90x23,0,46,6,182x23,91,46,7}]"
    (toLayoutUnits str, layoutUnits l) `shouldSatisfy` similarTuple

  it "Calculates tiled layout (9 panes)" $ do
    let x = Layout 0 LayoutTiled []
        l = fillLayoutUnits x 9 0 (273,69)
        str = "272x68,0,0[273x22,0,0{90x22,0,0,3,90x22,91,0,15,91x22,182,0,8},273x22,0,23{90x22,0,23,12,90x22,91,23,13,91x22,182,23,14},273x23,0,46{90x23,0,46,5,90x23,91,46,6,91x23,182,46,7}]"
    (toLayoutUnits str, layoutUnits l) `shouldSatisfy` similarTuple

  it "Calculates tiled layout (10 panes)" $ do
    let x = Layout 0 LayoutTiled []
        l = fillLayoutUnits x 10 0 (273,69)
        str = "272x67,0,0[273x16,0,0{90x16,0,0,3,90x16,91,0,15,91x16,182,0,16},273x16,0,17{90x16,0,17,8,90x16,91,17,12,91x16,182,17,13},273x16,0,34{90x16,0,34,14,90x16,91,34,5,91x16,182,34,6},273x18,0,51,7]"
    (toLayoutUnits str, layoutUnits l) `shouldSatisfy` similarTuple

  it "Calculates tiled layout (11 panes)" $ do
    let x = Layout 0 LayoutTiled []
        l = fillLayoutUnits x 11 0 (273,69)
        str = "272x67,0,0[273x16,0,0{90x16,0,0,3,90x16,91,0,15,91x16,182,0,16},273x16,0,17{90x16,0,17,17,90x16,91,17,8,91x16,182,17,12},273x16,0,34{90x16,0,34,13,90x16,91,34,14,91x16,182,34,5},273x18,0,51{90x18,0,51,6,182x18,91,51,7}]"
    (toLayoutUnits str, layoutUnits l) `shouldSatisfy` similarTuple

  -- 19... why not :P
  it "Calculates tiled layout (19 panes)" $ do
    let x = Layout 0 LayoutTiled []
        l = fillLayoutUnits x 19 0 (273,69)
        str = "271x69,0,0[273x13,0,0{67x13,0,0,3,67x13,68,0,15,67x13,136,0,16,69x13,204,0,17},273x13,0,14{67x13,0,14,18,67x13,68,14,19,67x13,136,14,20,69x13,204,14,21},273x13,0,28{67x13,0,28,22,67x13,68,28,23,67x13,136,28,24,69x13,204,28,25},273x13,0,42{67x13,0,42,8,67x13,68,42,12,67x13,136,42,13,69x13,204,42,14},273x13,0,56{67x13,0,56,5,67x13,68,56,6,137x13,136,56,7}]"
    (toLayoutUnits str, layoutUnits l) `shouldSatisfy` similarTuple






