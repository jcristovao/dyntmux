{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE FlexibleInstances #-}
{-# OPTIONS_GHC -fno-warn-orphans #-}

module Tmux.UtilSpec (spec) where

import Control.Applicative
import Control.Exception
{-import Control.Monad.Trans.Either-}

{-import qualified Data.Map as Map-}
import Test.Hspec
import Test.QuickCheck
import Test.QuickCheck.Instances

{-import Tmux.Types-}
{-import Data.Function-}
import Data.Text (Text)
import qualified Data.Text as T
import Data.Attoparsec.Text.Parsec
import Tmux.Util
{-import Tmux.Classes-}
{-import Tmux.Layout-}
{-import Tmux.Tmuxinator-}
{-import Tmux.Tmuxinator.Yaml-}

data TextToX = X Text deriving (Eq,Ord,Show)
data IntToY  = Y Int deriving  (Eq,Ord,Show)
data BoolToB = B Bool deriving (Eq,Ord,Show)

{-# ANN spec ("HLint: ignore Redundant do"::String) #-}
spec :: Spec
spec = describe "Tmux.Util" $ do
  it "Implements dropTillLast function" $ do
    dropTillLast 'a' "abc"    `shouldBe` "bc"
    dropTillLast 'a' "abca"   `shouldBe` ""
    dropTillLast 'a' "abcae"  `shouldBe` "e"
    dropTillLast 'a' "a1a2a3" `shouldBe` "3"
    dropTillLast 'a' "bcd"    `shouldBe` "bcd"
    dropTillLast 'a' ""       `shouldBe` ""

  it "Implements dropPrefix function" $ do
    dropPrefix "abc.def.ghi" `shouldBe` "ghi"
    dropPrefix "abc.a"       `shouldBe` "a"
    dropPrefix "a."          `shouldBe` ""
    dropPrefix "abc"         `shouldBe` "abc"
    dropPrefix ".."          `shouldBe` ""
    dropPrefix ""            `shouldBe` ""

  -- I won't test template haskell functions for now
  -- getTypeNames', getTypeTypes', getTypeCount', getDataMemberTypes

  it "Implements toTmuxFormat function" $ do
    toTmuxFormat "abc.dad" `shouldBe` "\t#{dad}"
    toTmuxFormat "abcAbc"  `shouldBe` "\t#{abc_abc}"
    toTmuxFormat "AbcAbc"  `shouldBe` "\t#{_abc_abc}"
    toTmuxFormat ""        `shouldBe` "\t"
    toTmuxFormat "abc.Abc" `shouldBe` "\t#{_abc}"
    toTmuxFormat "abc.Abc."`shouldBe` "\t"
    toTmuxFormat "ac.Ac.ad"`shouldBe` "\t#{ad}"
    toTmuxFormat "ac.Ac.aD"`shouldBe` "\t#{a_d}"

  it "Implements toTmuxFormat function (2)" $
    property $ \s -> (head . toTmuxFormat $ s) == '\t'

  it "Implements takeTillSep function" $ do
    parseOnly takeTillSep "aaa"         `shouldBe` Right "aaa"
    parseOnly takeTillSep "aaa\tbbb"    `shouldBe` Right "aaa"
    parseOnly takeTillSep "aaa\tbbb\ta" `shouldBe` Right "aaa"
    parseOnly takeTillSep "aaa\t"       `shouldBe` Right "aaa"
    parseOnly takeTillSep "\t"          `shouldBe` Right ""
    parseOnly takeTillSep ""            `shouldBe` Right ""

  it "Implements sep function" $ do
    parseOnly sep "\t"    `shouldBe` Right '\t'
    parseOnly sep "a"     `shouldBe`
        Left "(line 1, column 1):\nunexpected \"a\"\nexpecting \"\\t\"\nExpected separator"
    parseOnly sep ""      `shouldBe`
        Left "(line 1, column 1):\nunexpected end of input\nexpecting \"\\t\"\nExpected separator"

  it "Implements parseText function" $ do
    parseOnly (parseText X) "\tdef\t" `shouldBe` Right (X "def")
    parseOnly (parseText X) "\tdef"   `shouldBe` Right (X "def")
    parseOnly (parseText X) "\t"      `shouldBe` Right (X "")
    parseOnly (parseText X) "def\t"   `shouldBe`
        Left "(line 1, column 1):\nunexpected \"d\"\nexpecting \"\\t\"\nExpected separator"
    parseOnly (parseText X) ""        `shouldBe`
        Left "(line 1, column 1):\nunexpected end of input\nexpecting \"\\t\"\nExpected separator"

  it "Implements parseTextSingleton function" $ do
    parseOnly (parseTextSingleton X) "\tabc\t" `shouldBe` Right [X "abc"]
    parseOnly (parseTextSingleton X) "\tabc"   `shouldBe` Right [X "abc"]
    parseOnly (parseTextSingleton X) "\t"      `shouldBe` Right [X ""]
    parseOnly (parseTextSingleton X) "\t\t"    `shouldBe` Right [X ""]
    parseOnly (parseTextSingleton X) "abc\t"   `shouldBe`
        Left "(line 1, column 1):\nunexpected \"a\"\nexpecting \"\\t\"\nExpected separator"
    parseOnly (parseTextSingleton X) ""        `shouldBe`
        Left "(line 1, column 1):\nunexpected end of input\nexpecting \"\\t\"\nExpected separator"

  it "Implements parseNumPrefix function" $ do
    parseOnly (parseNumPrefix Y 'a') "\ta123" `shouldBe` Right (Y 123)
    parseOnly (parseNumPrefix Y 'a') "\tb"    `shouldBe`
        Left "(line 1, column 9):\nunexpected \"b\"\nexpecting \"a\""
    parseOnly (parseNumPrefix Y 'a') "\ta"    `shouldBe`
        Left "(line 1, column 10):\nunexpected end of input"
    parseOnly (parseNumPrefix Y 'a') "\tabcdefgh"    `shouldBe`
        Left "(line 1, column 10):\nunexpected \"b\""
    parseOnly (parseNumPrefix Y 'a') "\t"     `shouldBe`
        Left "(line 1, column 9):\nunexpected end of input\nexpecting \"a\""

  it "Implements parseNum function" $ do
    parseOnly (parseNum Y) "\t123"    `shouldBe` Right (Y 123)
    parseOnly (parseNum Y) "\t123\t"  `shouldBe` Right (Y 123)
    parseOnly (parseNum Y) "\t123a\t" `shouldBe` Right (Y 123)
    parseOnly (parseNum Y) "\tb"      `shouldBe`
        Left "(line 1, column 9):\nunexpected \"b\""
    parseOnly (parseNum Y) "\ta"      `shouldBe`
        Left "(line 1, column 9):\nunexpected \"a\""
    parseOnly (parseNum Y) "\tabcdefgh" `shouldBe`
        Left "(line 1, column 9):\nunexpected \"a\""
    parseOnly (parseNum Y) "\t"       `shouldBe`
        Left "(line 1, column 9):\nunexpected end of input"

  it "Implements parseInt function" $ do
    parseOnly parseInt "\t123"   `shouldBe` Right 123
    parseOnly parseInt "\t123\t" `shouldBe` Right 123
    parseOnly parseInt "\t123a\t"`shouldBe` Right 123
    parseOnly parseInt "\tb"     `shouldBe`
        Left "(line 1, column 9):\nunexpected \"b\"\nInvalid Number"
    parseOnly parseInt "\ta"    `shouldBe`
        Left "(line 1, column 9):\nunexpected \"a\"\nInvalid Number"
    parseOnly parseInt "\tabcdefgh"    `shouldBe`
        Left "(line 1, column 9):\nunexpected \"a\"\nInvalid Number"
    parseOnly parseInt "\t"     `shouldBe`
        Left "(line 1, column 9):\nunexpected end of input\nInvalid Number"

  it "Implements parseBool function" $ do
    parseOnly (parseBool B) "\t0"      `shouldBe` Right (B False)
    parseOnly (parseBool B) "\t1"      `shouldBe` Right (B True)
    parseOnly (parseBool B) "\t2"      `shouldBe` Right (B False)
    parseOnly (parseBool B) "\t123\t"  `shouldBe` Right (B True)
    parseOnly (parseBool B) "\t123a\t" `shouldBe` Right (B True)
    parseOnly (parseBool B) "\tb"      `shouldBe` Right (B False)
    parseOnly (parseBool B) "\ta"      `shouldBe` Right (B False)
    parseOnly (parseBool B) "\tabcdef" `shouldBe` Right (B False)
    parseOnly (parseBool B) "\t"       `shouldBe` Right (B False)
    parseOnly (parseBool B) "\t\t"     `shouldBe` Right (B False)

  it "Implements parseCommand'' function" $ do
    let pc = parseOnly parseCommand''
    pc "abc def"      `shouldBe` Right "abc"
    pc "abc \"def\""  `shouldBe` Right "abc"
    pc "\"def\"abc x" `shouldBe` Right "def"
    pc "a"            `shouldBe` Right "a"
    pc "\"def abc\" x"`shouldBe` Right "def abc"
    pc ""             `shouldBe`
        Left "(line 1, column 1):\nunexpected end of input\nexpecting \"\\\\\", \"\\\"\" or \" \"\nEmpty result:"

  it "Implements parseTillEndOfQuotes function" $ do
    let pt = parseOnly parseTillEndOfQuotes
    pt "abcd\""       `shouldBe` Right "abcd"
    pt "abcd"         `shouldBe`
      Left "(line 1, column 5):\nunexpected end of input\nexpecting \"\\\\\" or \"\\\"\""
    pt "a\""          `shouldBe` Right "a"
    pt ""             `shouldBe`
      Left "(line 1, column 1):\nunexpected end of input\nexpecting \"\\\\\" or \"\\\"\""

  it "Implements parseCommand' function" $ do
    let pc = parseOnly parseCommand'
    pc "abc def"      `shouldBe` Right ["abc","def"]
    {-pc "abc  def"     `shouldBe` Right ["abc","def"]-}
    pc "abc \"def\""  `shouldBe` Right ["abc","def"]
    pc "\"def\"abc x" `shouldBe` Right ["def","abc","x"]
    pc "a"            `shouldBe` Right ["a"]
    pc "\"def abc\""  `shouldBe` Right ["def abc"]
    pc "abc \""       `shouldBe` Right ["abc","\""]
    pc "\"def abc\"x" `shouldBe` Right ["def abc","x"]
    pc "\"def abc\" x"`shouldBe` Right ["def abc","x"]
    pc ""             `shouldBe` Right []

  it "implements parseCommand function" $ do
    let pc = parseCommand
    pc "abc def"     `shouldBe` ["abc","def"]
    pc "abc"         `shouldBe` ["abc"]
    pc "abc "        `shouldBe` ["abc"]
    evaluate (pc "") `shouldThrow` errorCall "Empty tmux command"
